/*
 * decaffeinate suggestions:
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
class Color {
	static generateRandomColor(mix, toHex) {
		// mix is the color we want to mix with, It should be a hex string
		// TAKEN FROM: https://stackoverflow.com/questions/43044/algorithm-to-randomly-generate-an-aesthetically-pleasing-color-palette
		if (toHex == null) { toHex = true; }
		let red   = Math.round(Math.random() * 256);
		let green = Math.round(Math.random() * 256);
		let blue  = Math.round(Math.random() * 256);

		// mix the color
		if (mix) {
			// We convert the mix color to decimal
			mix = mix.replace("#", "");
			const mix_red   = parseInt(mix[0] + mix[1], 16);
			const mix_green = parseInt(mix[2] + mix[3], 16);
			const mix_blue  = parseInt(mix[4] + mix[5], 16);
			red       = (red   + mix_red)   / 2;
			green     = (green + mix_green) / 2;
			blue      = (blue  + mix_blue)  / 2;
		}
		// convert to hex string
		if (toHex) {
			const hex_red   = red   .toString(16).toUpperCase();
			const hex_green = green .toString(16).toUpperCase();
			const hex_blue  = blue  .toString(16).toUpperCase();
			return `#${hex_red}${hex_green}${hex_blue}`;
		}
		return [red, green, blues];
	}
}

window.color = Color;