/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */

class SVGParser {
	static initClass() {

		this.STRING_TO_SVG = svgText => {
			// Converts the svg text into a svg DOM element
			const parser = new DOMParser();
			const doc    = parser.parseFromString(svgText, "image/svg+xml");
			return doc.documentElement;
		};

		this.PATH_TO_COORDONATES = pathStr => {
			// First we parse the path and get the commands
			const commands = this.GET_PATH_COMMANDS(pathStr);
			const points = [];
			let prev  = [0, 0];
			let first = [0, 0];
			for (let i = 0; i < commands.length; i++) {
				var point, value;
				const command = commands[i];
				const code = command.code.toLowerCase();
				if (code === "m") {
					point = command.end;
					prev = [point.x, point.y];
					points.push(prev);
				} else if (code === "l") {
					point = command.end;
					prev = [prev[0] + point.x, prev[1] + point.y];
					points.push(prev);
				} else if (code === "h") {
					({ value } = command);
					prev = [prev[0] + value, prev[1]];
					points.push(prev);
				} else if (code === "v") {
					({ value } = command);
					prev = [prev[0], prev[1] + value];
					points.push(prev);
				} else if (code === "z") {
					points.push(first);
				} else {
					console.warn("unrecognized command", code);
				}
				if (i === 0) {
					first = prev;
				}
			}
			return points;
		};
	}

	static GET_PATH_COMMANDS(pathStr) {
		// This function has been converted from https://github.com/MaxArt2501/d-path-parser
		// Using: http://js2.coffee/
		const re = {
			command: /\s*([achlmqstvz])/gi,
			number: /\s*([+-]?\d*\.?\d+(?:e[+-]?\d+)?)/gi,
			comma: /\s*(?:(,)|\s)/g,
			flag: /\s*([01])/g
		};
		var matchers = {
			'number'(must) {
				return +get('number', must);
			},
			'coordinate pair'(must) {
				const x = get('number', must);
				if ((x === null) && !must) {
					return null;
				}
				get('comma');
				const y = get('number', true);
				return {
					x: +x,
					y: +y
				};
			},
			'arc definition'(must) {
				const radii = matchers['coordinate pair'](must);
				if (!radii && !must) {
					return null;
				}
				get('comma');
				const rotation = +get('number', true);
				get('comma', true);
				const large = !!+get('flag', true);
				get('comma');
				const clockwise = !!+get('flag', true);
				get('comma');
				const end = matchers['coordinate pair'](true);
				return {
					radii,
					rotation,
					large,
					clockwise,
					end
				};
			}
		};
		let index = 0;
		const commands = [];

		const makeCommand = function(obj) {
			obj.code = cmd;
			obj.relative = relative;
			return obj;
		};

		var get = function(what, must) {
			re[what].lastIndex = index;
			const res = re[what].exec(pathStr);
			if (!res || (res.index !== index)) {
				if (!must) {
					return null;
				}
				throw Error(`Expected ${what} at position ${index}`);
			}
			index = re[what].lastIndex;
			return res[1];
		};

		const getSequence = function(what) {
			const sequence = [];
			let matched = undefined;
			let must = true;
			while ((matched = matchers[what](must))) {
				sequence.push(matched);
				must = !!get('comma');
			}
			return sequence;
		};

		while (index < pathStr.length) {
			var cmd = get('command');
			const upcmd = cmd.toUpperCase();
			var relative = cmd !== upcmd;
			let sequence = undefined;
			switch (upcmd) {
				case 'M':
					sequence = getSequence('coordinate pair').map(function(coords, i) {
						if (i === 1) {
							cmd = relative ? 'l' : 'L';
						}
						return makeCommand({end: coords});
					});
					break;
				case 'L': case 'T':
					sequence = getSequence('coordinate pair').map(coords => makeCommand({end: coords}));
					break;
				case 'C':
					sequence = getSequence('coordinate pair');
					if (sequence.length % 3) {
						throw Error(`Expected coordinate pair triplet at position ${index}`);
					}
					sequence = sequence.reduce((function(seq, coords, i) {
						const rest = i % 3;
						if (!rest) {
							seq.push(makeCommand({cp1: coords}));
						} else {
							const last = seq[seq.length - 1];
							last[rest === 1 ? 'cp2' : 'end'] = coords;
						}
						return seq;
					}), []);
					break;
				case 'Q': case 'S':
					sequence = getSequence('coordinate pair');
					if (sequence.length & 1) {
						throw Error(`Expected coordinate pair couple at position ${index}`);
					}
					sequence = sequence.reduce((function(seq, coords, i) {
						const odd = i & 1;
						if (!odd) {
							seq.push(makeCommand({cp: coords}));
						} else {
							const last = seq[seq.length - 1];
							last.end = coords;
						}
						return seq;
					}), []);
					break;
				case 'H': case 'V':
					sequence = getSequence('number').map(value => makeCommand({value}));
					break;
				case 'A':
					sequence = getSequence('arc definition').map(makeCommand);
					break;
				case 'Z':
					sequence = [ { code: 'Z' } ];
					break;
			}
			commands.push.apply(commands, sequence);
		}
		return commands;
	}
}
SVGParser.initClass();

window.svg = SVGParser;