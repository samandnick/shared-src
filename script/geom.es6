

export function pointInRect(point, rect) {
	// TODO: Should be refactored into a geometry module
	const is_in_x = (point.x > rect.x) && (point.x < (rect.x + rect.w));
	const is_in_y = (point.y > rect.y) && (point.y < (rect.y + rect.h));
	if (is_in_x && is_in_y) {
		return true;
	} else {
		return false;
	}
}

export function scaleVector(vector, scale) {
	if ((vector.x === 0) && (vector.y === 0)) {
		return {x:0,y:0};
	}
	const direction = this.getVectorDirection(vector);
	const val = {
		x: (Math.cos(direction) * scale) || 0,
		y: (Math.sin(direction) * scale) || 0
	};
	return val;
}

export function getVectorDirection(vector) {
	return Math.atan2(vector.y, vector.x);
}

export function circlePointCollision(circle, point){
	if (circle.r == 0){return false}
	var dx = circle.x - point.x
	var dy = circle.y - point.y
	return dx * dx + dy * dy <= circle.r * circle.r
}

export function circleCircleCollision(c1, c2){
	if (c1.r == 0){return false}
	var dx = c1.x - c2.x
	var dy = c1.y - c2.y
	return dx * dx + dy * dy <= (c1.r + c2.r) * (c1.r + c2.r)
}

export function circleCircleTangent(c1, c2){
	// Algorithm from : http://forums.phpfreaks.com/topic/98615-common-tangents-of-two-circles/
	// pt1, pt2, r1, r2
	// Default is r1 > r2
	let r1  = c1.r
	let r2  = c2.r
	let pt1 = [c1.x, c1.y]
	let pt2 = [c2.x, c2.y]
	let cen1x = undefined
	let cen2x = undefined
	let cen1y = undefined
	let cen2y = undefined
	if (r1 < r2){
		var temp = r1
		r1       = r2
		r2       = temp
		cen1x = pt2[0]
		cen1y = pt2[1]
		cen2x = pt1[0]
		cen2y = pt1[1]
	} else{
		cen1x = pt1[0]
		cen1y = pt1[1]
		cen2x = pt2[0]
		cen2y = pt2[1]
	}
	// Distance between two centres
	var cendistance = Math.sqrt ((cen1x-cen2x)*(cen1x-cen2x) + (cen1y-cen2y)*(cen1y-cen2y))
	var a = cendistance*r2 / Math.max (r1-r2, 0.0000001)
	// Coordinates at the vertex
	var vx = cen2x + (cen2x - cen1x)/cendistance*a
	var vy = cen2y + (cen2y - cen1y)/cendistance*a
	// Angle between line joining two centres and the x-axis
	var pheta = Math.atan2 (cen1y-cen2y, cen1x-cen2x)
	// Angle between line joining two centres and the common tangent
	var phi = Math.asin(r2/a)
	// One tangent is at the angle (pheta-phi) with x-axis; the other is (pheta+phi)
	var tan1angle = pheta - phi
	var tan2angle = pheta + phi
	// Find the length of tangents (to C1 and C2)
	var criterion = a*a-r2*r2
	if (criterion < 0){
		return [[pt1[0],pt1[1]], [pt1[0],pt1[1]], [pt1[0],pt1[1]], [pt1[0],pt1[1]]]
	}
	var t1 = Math.sqrt(criterion)
	var t2 = Math.sqrt((cendistance+a)*(cendistance+a)-r1*r1)
	// Find the points of contact; this sequence is used to form a polygon
	var poc1x = vx + Math.cos(tan1angle)*t1
	var poc1y = vy + Math.sin(tan1angle)*t1
	var poc2x = vx + Math.cos(tan1angle)*t2
	var poc2y = vy + Math.sin(tan1angle)*t2
	var poc3x = vx + Math.cos(tan2angle)*t2
	var poc3y = vy + Math.sin(tan2angle)*t2
	var poc4x = vx + Math.cos(tan2angle)*t1
	var poc4y = vy + Math.sin(tan2angle)*t1
	return [
		{x:poc1x, y:poc1y},
		{x:poc2x, y:poc2y},
		{x:poc3x, y:poc3y},
		{x:poc4x, y:poc4y}
	]
}

export function lerp3d(p1, p2, t) {
	return {
		x : p1.x + (p2.x - p1.x) * t,
		y : p1.y + (p2.y - p1.y) * t,
		z : p1.z + (p2.z - p1.z) * t,
	}
}

export function distance(p1, p2){
	let dx = Math.abs(p1.x - p2.x)
	let dy = Math.abs(p1.y - p2.y)
	let dz = Math.abs(p1.z - p2.z) || 0
	return Math.sqrt(dx * dx + dy * dy + dz * dz)
}