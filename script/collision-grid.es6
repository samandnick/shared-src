

class Grid {

	constructor (collisionMatrix) {
		this.collisionMatrix = {
			player   : ["wall", "box"],
			box      : ["wall"],
			wall     : [],
			conveyor : [],
		}
		this.entities = {}
		this.cells = {}
		this._prevTime = undefined
	}

	add(entity){
		this.entities[entity.id] = entity
	}

	remove(entity){
		delete this.entities[entity.id]
	}

	update(delta=16){
		var now = Date.now()
		if(!delta){delta = (now - this._prevTime) || 0}

		this._prevTime = now
	}

}
Grid.INIT_CLASS()


class Entity {

	static INIT_CLASS() {
		this._NEXT_ID = 0
	}

	static GET_ID() {
		let id = this._NEXT_ID
		this._NEXT_ID += 1
		return id
	}

	constructor() {
		this.id = Entity.GET_ID()
		this.speed          = 1 //cells per second
		this.weight         = 1 // determines what can be pushed or what can push
		this.pushSameWeight = true
		this.types = ["player"]
		this.shape = [
			[0],
		]
		this.x  = 0
		this.y  = 0
		this.progress = 0
		this.actions = []
		this.queueSize = 1
	}

	move(x,y,speed){
		// Use sparingly might cause bugs
		if(this.actions.length < this.queueSize){
			this.actions.push({x,y,speed})
		}
	}

	moveLeft(v,speed){
		this.move(v,0,speed)
	}

	moveRight(v,speed){
		this.move(-v,0,speed)
	}

	moveUp(v,speed){
		this.move(0,v,speed)
	}

	moveDown(v,speed){
		this.move(0,-v,speed)
	}

	update(){

	}

}
Enitity.INIT_CLASS()