import * as parts     from "./core/parts.js"
import * as tools     from "./core/tools.js"
import * as animation from "./core/animation.js"
import * as three     from "./three-0.91.0.js"

export class World {

	static INIT_CLASS() {
	}

	constructor(canvas) {
		// We want to give it a pixelated look
		this.viewport     = new Viewport(canvas, this)
		this.player       = null
		this.entities     = {}
		this.links        = []
		this._prevTime    = undefined
		this.animator     = new animation.Animator()
		this.animator.update.then(this._update, this)
		this.start()
	}

	resize() {
		this.viewport.resize()
	}

	start (){
		this.animator.start()
	}

	stop () {
		this.animator.stop()
	}

	_update(delta=16) {
		// We simulate the world
		for (let key in this.entities){
			let entity = this.entities[key]
			entity.update(delta)
		}
		this.viewport.render()
	}

}
World.INIT_CLASS()


export class Viewport {

	constructor(canvas, world, gridWidth, gridHeight, x, y, scale="contain") {
		this.w          = 1
		this.h          = 1
		this.scale      = scale
		this.world      = world
		this.scene      = new three.Scene()
		this.camera     = new three.PerspectiveCamera(75, 1, 0.1, 1000)
		this.renderer     = new three.WebGLRenderer({canvas})
		this.canvas       = canvas
		this._prepared    = {}
		this.resize()
		this.test()
	}

	resize () {
		console.log("resize")
		var bounds = this.canvas.$offset()
		this.w          = bounds.w
		this.h          = bounds.h
		this.camera.aspect = this.w/this.h
		this.camera.updateProjectionMatrix()
		this.renderer.setSize(this.w, this.h, false)
	}

	test () {
		var geometry = new three.BoxGeometry( 1, 1, 1 )
		var material = new three.MeshBasicMaterial( { color: 0x00ff00 } )
		this.cube = new three.Mesh( geometry, material )
		this.scene.add(this.cube)
		this.camera.position.z = 5
	}

	project(position, y) {
		// Projects a game position to the screen
		let x = position
		if (_.isObject(position)) {
			x = position.x
			y = position.y
		}
		x = (x * this.scale) + (this.x * this.scale) + (this.bounds.w/2);
		// We flip the y axis
		y = (y * this.scale * -1) + (this.y * this.scale) + (this.bounds.h/2);
		return [x, y];
	}

	unproject(position, y) {
		// Projects a pixel on the screen to a game position
		// TODO
	}

	prepareEntity(entity){
		let texture = entity.getTexture()
	}

	render() {
		// We make sure all the entities are prepared
		for (let key in this.world.entities){
			let entity = this.world.entity[key]
			if (!this._prepared[key]){
				this.prepareEntity(entity)
				this._prepared[key] = true
			}
			// We update the entities
			entity.render(this)
		}
		// We render the result
		if (this.cube){
			this.cube.rotation.x += 0.1
			this.cube.rotation.y += 0.1
		}
		this.renderer.render(this.scene, this.camera)
	}

}

export class Entity {

	static INIT_CLASS() {
		this.ASSETS = {}
	}

	constructor(x, y, w, h) {
		this.texture     = null
		this.state       = "default"
		this.graphics    = {}
		this.world       = null
		this.id          = null
		this.x           = x
		this.y           = y
		this.w           = w
		this.h           = h
	}

	getTexture(resolution=16) {
		const graphics = new PIXI.Graphics()
		graphics.beginFill(0x999999, 1)
		// We draw a rectangle
		const rect = [
			[0                , resolution*this.h],
			[resolution*this.w, resolution*this.h],
			[resolution*this.w, 0],
			[0                , 0]
		]
		for (let i = 0; i < rect.length; i++) {
			const coord = rect[i]
			const x = coord[0]
			const y = coord[1]
			if (i === 0) {
				graphics.moveTo(x, y)
			} else {
				graphics.lineTo(x, y)
			}
		}
		graphics.endFill()
		this.graphics["default"] = graphics
		this.setState(("default"))
		return graphics
	}

	setState(state) {
		const graphic = this.graphics[this.state]
		if (graphic) {
			graphic.visible = false
			if (graphic.stop) {
				graphic.stop()
			}
		}
		const new_graphic = this.graphics[state]
		if (new_graphic) {
			graphic.visible = true
			if (graphic.play) {
				graphic.play()
			}
		}
		this.state = state
	}

	getRect() {
		return {
			x : this.x,
			y : this.y,
			w : this.w,
			h : this.h
		};
	}

	update(delta) {
		const pos = this.getPosition()
		this.position = pos
	}

	getPosition() {
		const sim_x = bpos.get_x()
		const sim_y = bpos.get_y()
		return [sim_x, sim_y]
	}

	render(viewport) {
		const graphic = this.graphics["default"]
		if (graphic) {
			// We render the animating sprite
			const pos = viewport.project(this.getPosition())
			graphic.x = pos[0]
			graphic.y = pos[1]
			const scale = viewport.scale * viewport.pixelScale
			// We devide by 16 since thats the resolution per "tile"
			graphic.scale = new PIXI.Point(scale, scale)
		}
	}

}
Entity.INIT_CLASS()

// FOR DEBUGGING, TO REMOVE
window.three = three
