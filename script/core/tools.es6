// -----------------------------------------------------------------------------
//
// Module: Tools
// Status: Unfinished
//
// -----------------------------------------------------------------------------
// A collection of useful helper functions, for strings, lists, objects,
// and math

// ============================================================================
// STRING OPERATIONS
// ============================================================================

export function capitalize (string){
	return string.charAt(0).toUpperCase() + string.slice(1)
}

// ============================================================================
// SET/LIST/MAP OPERATIONS
// ============================================================================

export function access(object, ...path){
	// We search an object for a value
	if (_.isArray(path[0])){path = path[0]}
	let value = object
	for(let key of path){
		if (_.isUndefined(value)){return value}
		value = value[key]
	}
	return value
}

export function bucket (key, value, map={}) {
	// NOTE: MUTATES map
	// Adds a value to the map turning the value into an array if it exists already
	if (!map[key]) {
		map[key] = value
	} else {
		// If the value exists we convert it to an array and insert the new element
		if (!_.isArray(map[key])) {
			map[key] = [map[key]]
		}
		map[key].push(value)
	}
	return map
}

export function ensureArray (value) {
	if (_.isUndefined(value)) {return []}
	if (_.isArray(value)) {return value}
	return [value]
}

export function merge (...objects) {
	// Merge the objects into a new list/map
	if (_.isArray(objects[0])){
		// FIXME: this should mutate first argument
		return _.union(...objects)
	}else {

		return Object.assign({},...objects)
	}
}

export function isIn (value, list){
	for (let k in list){
		let v = list[k]
		if (v == value){return true}
	}
	return false
}

export function arrayToMap (array, key=undefined, value=undefined) {
	// converts an array to a map
	var map = {}
	for (let v of array) {
		let val = v
		if(_.isFunction(value)) {
			val = value(v)
		} else if (!_.isUndefined(value)) {
			val = value
		}
		if(_.isFunction(key)) {
			map[key(v)] = val
		} else if (!_.isUndefined(key)) {
			map[key] = val
		} else if (_.isString(v)) {
			map[v] = val
		} else {
			console.warn("arrayToMap, cannot convert type to key")
		}
	}
	return map
}

export function items (object) {
	// NOTE: only used in controls at the moment
	let items = []
	for (let k in object) {
		items.push({k:k, v:object[k]})
	}
	return items
}

export function loop (list, callback) {
	// DEPRICATED
	let _j=list instanceof Array ? list : Object.getOwnPropertyNames(list||{})
	let _l=_j.length
	for (let _k=0;_k<_l;_k++){
		// We check if its the same, if it is its an array if not its a
		let key=(_j===list)?_k:_j[_k]
		let value=list[key]
		callback(value, key)
	}
}

// ============================================================================
// MATH
// ============================================================================

export function scale (val, range, destination) {

	// Formula
	//        (b-a)(x - min)
	//f(x) = --------------  + a
	//          max - min
	// TODO: Rename variables
	if (_.isNumber(range)){range = [0,range]}
	if (_.isNumber(destination)){destination = [0,destination]}
	var top = (destination[1] - destination[0]) * (val - range[0])
	var bottom = range[1] - range[0]
	return (top/bottom) + destination[0]
}

export function clamp (val, min, max){
	return Math.max(Math.min(val, max), min)
}

export function mod (num, mod){
	return ((num % mod) + mod) % mod
}

export function seededRandom (min=0, max=1, int=false, seed=6){
	// Algorithm from: http://indiegamr.com/generate-repeatable-random-numbers-in-js/
	seed = (seed * 9301 + 49297) % 233280
	var val = seed / 233280
	val = min + val * (max - min)
	if (int){val = Math.round(val)}
	return val
}

export function seededShuffle(array, size, seed=6) {
	// Algorithm from: lodash (baseShuffle)
  var index = -1,
      length = array.length,
      lastIndex = length - 1;

  size = size === undefined ? length : size;
  while (++index < size) {
    var rand = seededRandom(index, lastIndex, true, seed),
        value = array[rand];
    array[rand] = array[index];
    array[index] = value;
  }
  array.length = size;
  return array;
}

// ----------------------------------------------------------------------------
//
// TEMPLATE
//
// ----------------------------------------------------------------------------

export class Template {
// A Template are used to manage lists of items/instances,
// that change over the lifespan of the application
// Templates manages these instances using create/update/delete methods
// Templates will re-use instances according to there key or index, this is helpful
// for performance and animations and creates a de-facto api for lists.
// NOTE: a `scope` parameter may be passed which will be used as `this` in the actions.

	constructor(actions={}, scope=this) {
		this.scope     = scope
		this.data      = {}
		this.instances = {} // a map of {key:{data, instance}}
		this.actions   = {
			create:undefined || actions.create,
			change:undefined || actions.change, // Triggers when created or updated (not triggered when deleted)
			update:undefined || actions.update, // Triggers when the instance is updated (not triggered on creation)
			delete:undefined || actions.delete,
		}
	}

	set(data){
		// This method will trigger the proper create/update/delete instance methods for each instance.
		let prev_data = this.data
		let created   = _.omit(data,  _.keys(prev_data))
		let updated   = _.pick(data,  _.keys(prev_data))
		let deleted   = _.omit(prev_data, _.keys(data))
		// We create the new uis for the new elements
		for(let key in created){
			let datum = created[key]
			let instance = this._create(datum, key, data)
			if (_.isUndefined(instance)){console.warn("Template:create must return an instance value, this can be a `part`, a `node` or anything else")}
			// Add it to the instances
			this.instances[key] = {data:datum, instance}
		}
		// We update the existing elements
		for(let key in updated){
			let datum = updated[key]
			// We get the previous data
			this._update(datum, this.instances[key].instance, key, data, prev_data[key])
		}
		// and we delete the deleted elements
		for(let key in deleted){
			let datum = deleted[key]
			// We trigger the delete callbacks
			this._delete(prev_data[key], this.instances[key].instance, key, data)
			// We remove the instance from the instances
			delete this.instances[key]
		}
		this.data = data
		return this.instances
	}

	_create(datum, key, data) {
		let instance = null
		if (this.actions.create){
			instance = this.actions.create.call(this.scope, datum, this, key, data)
		} else {
			console.warn("No create method defined for template")
		}
		this._change(datum, instance, key, data, null)
		// NOTE: This method needs to return an object, that will be passed to onUpdate and onDelete
		return instance
	}

	_update(datum, instance, key, data, prevData) {
		if (this.actions.update){
			this.actions.update.call(this.scope, datum, instance, key, data, prevData)
		}
		this._change(datum, instance, key, data, prevData)
	}

	_change(datum, instance, key, data, prevData) {
		if (this.actions.change){
			this.actions.change.call(this.scope, datum, instance, key, data, prevData)
		} else {
			console.warn("No change method defined for template")
		}
	}

	_delete(prevData, instance, key, data) {
		if (this.actions.delete){
			this.actions.delete.call(this.scope, prevData, instance, key, data)
		} else {
			// console.warn("No delete method defined for template")
		}
	}

}

// ----------------------------------------------------------------------------
//
// EMITTER
//
// ----------------------------------------------------------------------------

// TODO: Visualizer
// TODO: onFail

export class Emitter {
// Emitters are a kind of event that keeps track of its previous state.
// this can be useful since we can stop duplicate events unless `forced`.
// Emitters can also be chained using the `then` method.
// Chaining Emitters can be useful since they will absorb circular events,
// Basically when `Emitter.set` is called it will make sure that it doesn't
// get re-triggered by another emitter in the chain, preventing infinite cycles.

	static INIT_CLASS() {
		this._PROCESSING = {}
		this.DEFAULT_OPTIONS = {
			deep   : false, // Deep comparison of values to see if the value has changed (prevents cycles)
			force  : false, // Force the action to trigger even if the value is the same
		}
		this.DEFAULT_ACTION_OPTIONS = {
			position : -1,    // What order the actions will be processed in
			count    : -1,    // Number of times the action will trigger before being removed
			wait4All : false, // Causes the action to wait until all the triggers are set
		}
	}

	static _NEXT_ID () {
		if (_.isUndefined(this.nextId)){this.nextId = 0}
		let id = this.nextId
		this.nextId += 1
		return id
	}

	constructor(endpoints={default:undefined},lifespan=-1,options=undefined) {
		// We convert the endpoints to a proper map
		var size = _.size(endpoints)
		if (_.isArray(endpoints)){
			// If `endpoints` is an array we set the values to undefined
			let d = endpoints
			endpoints = {}
			for(let v of endpoints) {
				endpoints[v] = undefined
			}
		} else if (_.isNumber(endpoints)){
			// If `endpoints` is a number, we simply set the size
			size = endpoints
			endpoints = {}
		}
		this.id                 = Emitter._NEXT_ID() // used to prevent cycles
		this._cache             = endpoints // Map containing the current values of the emitter
		this._on                = [] // List of `{action, count, wait4all}`, action can be a callback or an emitter
		this.options            = _.defaults(options, Emitter.DEFAULT_OPTIONS)
		this._endpointCount     = _.mapValues(endpoints, (v)=>{return 0})
		this.lifespan           = lifespan
		this.size               = size
	}

	get(endpoint="default"){
		return this._cache[endpoint]
	}

	getAll(){
		return this._cache
	}

	hasTriggered(endpoint=undefined){
		if(!endpoint){
			return _.min(_.values(this._endpointCount)) != 0 && _.size(this._endpointCount) == this.size
		} else {
			return this._endpointCount[endpoint] != 0
		}
	}

	set(value, endpoint="default", options={}) {
		// options: force:boolean, deep:boolean, source:object
		// Triggers the event, that will endpoint all the callbacks set by `on`
		// We only run the callbacks if the value has changed or we force it

		// We show a warning if the endpoint is not in the cache
		if (this.size == _.size(this._cache) && !(endpoint in this._cache)){console.warn("cannot then for", endpoint)}
		// We determine if we looped, by checking if a sync or async emitter endpointed this set
		let processing = Emitter._PROCESSING
		let is_root = _.size(processing) == 0
		if (processing[this.id]) {
			// If we have previously processing this emitter we have looped
			// console.warn("Canceled loop")
			return
		} else {
			// If not we register it
			processing[this.id] = true
		}
		let opts = _.defaults(options, this.options)
		let to_remove = []
		// We update the cache (we need to do it before the callback is run, if not chained events might recurse forever)
		let cache = _.clone(this._cache)
		let prev = cache[value]
		this._cache[endpoint] = value
		// We count the endpoints
		if (_.isUndefined(this._endpointCount[endpoint])){this._endpointCount[endpoint] = 0}
		this._endpointCount[endpoint] += 1
		let endpoint_count     = _.max(_.values(this._endpointCount))
		if (endpoint_count > this.lifespan && this.lifespan != -1) {console.warn("Emitter endpoint count exceeded lifespan!")}
		let has_all_triggered = this.hasTriggered()
		// We check to see if it has changed
		let has_changed = opts.deep ? !_.isEqual(prev, value) : prev != value
		// If it changed or we are force the set, then endpoint the actions
		if (has_changed || opts.force) {
			for (let tuple of this._on) {
				let {action, count, wait4all, scope} = tuple
				let has_remaining_count = count == -1 || count > 0
				let is_waiting_4_all    = wait4all && !has_all_triggered
				if (has_remaining_count && !is_waiting_4_all) {
					// We run the action
					let set_value = value
					if (this.size > 1){set_value = this._cache}
					if (action instanceof Emitter){
						action.set(set_value, endpoint, options)
					} else if (action instanceof Function) {
						// Triggering an emitter in the callback could create cycles
						action.call(scope, set_value, endpoint, this._cache, prev)
					}
					// We decrement the count if it's not -1
					if (count > 0) {
						tuple.count -= 1
						if (tuple.count == 0) {
							// We mark the tuple to be removed
							to_remove.push(tuple)
						}
					}
				}
			}
			// We remove the tuples
			for (let tuple of to_remove) {
				let i = this._on.indexOf(tuple)
				if (i !== -1) {
					this._on.splice(i,1)
				} else {
					// this shouldn't ever endpoint
					console.warn("Couldn't remove action from event")
				}
			}
		}
		// The emitter is complete so it is no longer being processed
		delete processing[this.id]
		if (is_root && _.size(processing) != 0){
			// The processing map should be empty here
			console.warn("emitter stack did something weird", processing)
			Emitter._PROCESSING = {}
		}
	}

	then(action, scope=this, options={}){
		// triggers the action whenever the emitter gets set.
		let opts = _.defaults(options, Emitter.DEFAULT_ACTION_OPTIONS)
		let trigger = {action, count:opts.count, wait4all:opts.wait4all, scope}
		if (opts.position == -1) {
			this._on.push(trigger)
		} else {
			this._on.splice(opts.position, 0, trigger)
		}
	}

	afterAll(action, scope=this){
		// Triggers when all the values are set at least once and will trigger normally afterwards
		this.then(action, scope, {wait4all:true})
	}

	afterAllOnce(action, scope=this){
		// Triggers when all the values are set at least once and will trigger normally afterwards
		this.then(action, scope, {wait4all:true, count:1})
	}

	once(action, scope=this){
		// Binds a action that will only trigger one time
		// shorthand for `then` with a count of `1`
		this.then(action, scope, {count:1})
	}

	cancel(action=undefined) {
		// Removes the `action` handler if no action is passed
		// it will remove all the handlers.
		if (_.isUndefined(action)){
			// If no action is defined, we remove all of them
			this._on = []
		} else {
			// We find the index of the bound action
			let index = _.findIndex(this._on, (h) => {
				return h.action == action
			})
			// We remove it
			if (index >= 0){
				this._on.splice(index,1)
			} else {
				console.warn(`Emitter: could not remove handler, it is not bound`)
			}
		}
	}

}
Emitter.INIT_CLASS()

export function reducer(endpoints={}){
	return new Emitter(endpoints)
}

export function emitter(initial=undefined){
	return new Emitter({default:initial})
}

export function future(value=undefined) {
	let emitter = new Emitter(1, 1)
	if(!_.isUndefined(value)){
		setTimeout(()=>{emitter.set(value)},1)
	}
	return emitter
}

export function rendezVous(count){
	// FIXME: It would be better if we could pass a count instead of defaults
	return new Emitter(count, 1)
}

export function timeout(delay, action){
	let emitter = new Emitter(1, 1)
	emitter.then(action)
	return setTimeout(()=>{
		emitter.set(true)
	}, delay)
}

export function interval(delay, action){
	let emitter = new Emitter(1, -1, {force: true})
	emitter.then(action)
	return setInterval(()=>{
		emitter.set(true)
	}, delay)
}
