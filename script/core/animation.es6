// ----------------------------------------------------------------------------
//
// Module: animation
// Status: Unfinished
//
// ----------------------------------------------------------------------------
// Collection of classes related to animations

import * as parts from "./parts.js"
import * as tools from "./tools.js"

export class Animator {

	constructor () {
		this.update    = new tools.Emitter()
		this.isRunning = false
		this._prev     = undefined
	}

	start() {
		// If its already running we do nothing
		if(this.isRunning){return}
		this.isRunning = true
		this._update ()
	}

	stop(){
		this.isRunning = false
		this._prev = undefined
	}

	_update(){
		let now = Date.now()
		if (!this.isRunning){return}
		let delta = (now - this._prev) || 0
		// We trigger the update function passing the delta time
		this._prev = now
		this.update.set(delta)
		requestAnimationFrame (() => {this._update()})
	}

}
