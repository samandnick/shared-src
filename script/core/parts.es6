// -----------------------------------------------------------------------------
//
// Module: Parts
// Status: Mostly done
//
// -----------------------------------------------------------------------------
// A collection of classes to be used as building blocks for
// any small, medium and potentially large applications.
// TODO
// - Find a better way to do INIT_CLASS (SuperJS project)
// - Add event joining, chaining and forking
// WOULD BE NICE
//  - Emitter.VIZUALIZE
// CONSIDERATION
// - Recursive resizing
// - bindWithLastEvent, basically `on` but that will trigger if the even has already fired once
// MISSING FEATURES from widgets
// - states, show/hide

import * as tools   from "./tools.js"
import * as format  from "./format.js"

export const PARTS = {} // Map containing the root parts (useful for debugging)

export const MARKERS = {
	part       : "data-part",
	node       : "data-node",
	template   : "data-template",
	options    : "data-options", // used with: part
	format     : "data-format",  // used with: node
	events     : "data-on",      // used with: node, template
	path       : "data-path"     // used with: node, part, template
}

// ----------------------------------------------------------------------------
//
// NODE (DOM element wrapper)
//
// ----------------------------------------------------------------------------

export class Node {
	// The node is a singleton constructor class
	// The node class is a collections of static node functions
	// the main purpose of the class is to behave as a constructor
	// class that is used to extend DOM nodes with additional functionality

	static FIND_MANY(query, start=document){
		// finds and extends many dom nodes with jquery life functions
		// Note this returns a normal list of nodes, unlike jquery
		let nodes = start.querySelectorAll(query)
		let result = []
		for (let node of nodes) {
			result.push(Node.FIND(node))
		}
		return result
	}

	static GET_PATH(node){
		let path = node.$attr(MARKERS.path)
		if (path == ""){return []}
		if (!_.isUndefined(path)){
			return path.split(".")
		}
		return undefined
	}

	static FIND(query, start=document){
		// finds and extends a dom node with jquery life functions
		let node = query
		if (_.isString(query)){
			node = start.querySelector(query)
		}
		return this.WRAP(node)
	}

	static CLONE(node, anchor=undefined) {
		// Clones a template remove any template attributes and appends it after the template node
		let clone    = node.$clone()
		// We insert the element into the DOM
		if (anchor) {
			anchor.before(clone)
		}
		return clone
	}

	static WRAP(node){
		if (node.$isNode){return node}
		if (node){
			node.$css         = Node._CSS
			node.$find        = Node._FIND
			node.$findMany    = Node._FIND_MANY
			node.$clone       = Node._CLONE
			node.$attr        = Node._ATTR
			node.$hasClass    = Node._HAS_CLASS
			node.$toggleClass = Node._TOGGLE_CLASS
			node.$removeClass = Node._REMOVE_CLASS
			node.$addClass    = Node._ADD_CLASS
			node.$offset      = Node._OFFSET
			node.$text        = Node._TEXT
			node.$before      = Node._INSERT_BEFORE
			node.$path        = Node.GET_PATH(node)
			node.$format      = node.$attr(MARKERS.format)
			node.$isNode      = true
		}
		return node
	}

	static FIND_PARENT_WITH_ATTR (node, attributes, root){
		// search ancestors for attributes starting from a node up until it hits
		// root. This is mainly used in the parts to efficiently find nodes
		let parent = node.parentElement
		attributes = tools.ensureArray(attributes)
		while (parent) {
			if (parent == root) {break}
			let end = false
			for (let a of attributes){
				if (parent.hasAttribute(a)){
					end = true
					break
				}
			}
			if (end){break}
			parent = parent.parentElement
		}
		return parent
	}

	static IS_NODE(node){
		return node instanceof Element
	}

	static _INSERT_BEFORE (newNode){
		this.parentNode.insertBefore(newNode, this)
	}

	static _CSS(name, value){
		if (_.isString(name)) {
			if (_.isUndefined(value)) {
				// We return the value
				let style = document.defaultView.getComputedStyle(this, null)[name]
				if (!_.isUndefined(style)) {return style}
				return undefined
			} else {
				// We set the value
				if (_.isNumber(value)){value = value + "px"}
				this.style[name] = value
			}
		} else {
			// We treat the name as a map of {key:value}
			for (let k in name) {this.$css(k, name[k])}
		}
		return this
	}

	static _TEXT (value){
		if (_.isUndefined(value)) {
			return this.textContent
		} else {
			this.textContent = value
			return this
		}
	}

	static _CLONE (){
		return Node.FIND(this.cloneNode(true))
	}

	static _ATTR(name, value){
		if (_.isString(name)) {
			if (arguments.length == 1) {
				// We get the style value
				if (this.hasAttribute(name)) {
					return this.getAttribute(name)
				}
				return undefined
			} else {
				// We set the style value
				if (value === null) {
					if (this.hasAttribute(name)) {
						this.removeAttribute(name)
					}
				} else {
					this.setAttribute(name, value)
				}
			}
		} else if (name) {
			for (var k in name) {this.attr(k, name[k])}
		}
		return this
	}

	static _HAS_CLASS(name) {
		let lc = (name||"").length;
		if (! _.isUndefined(this.classList)) {
			return this.classList.contains(name)
		} else {
			// For SVG
			let c   = this.className || ""
			if (c & c.length > 0) {
				let m   = c.indexOf(name)
				if (m >= 0) {
					let la  = c.length || 0
					let p   = m - 1
					let n   = m + lc + 1
					// If the className is surrounded by spaces or start/end, then
					// we have a match.
					if (((m == 0)  || (c[p] == " ")) && ((m == la) || (c[n] == " "))) {
						return true
					}
				}
			}
		}
		return false;
	}

	static _TOGGLE_CLASS(name, value){
		if(_.isUndefined(value)) {
			if (this.$hasClass(name)) {
				this.$removeClass(name)
			}else{
				this.$addClass(name)
			}
		} else if ( value && !this.$hasClass(name)) {
			this.$addClass   (name)
		} else if (!value &&  this.$hasClass(name)) {
			this.$removeClass(name)
		}
		return this
	}

	static _REMOVE_CLASS(name){
		if (this.classList) {
			this.classList.remove(name)
		} else {
			var c   = this.getAttribute("class")
			if (c && c.length > 0) {
				var m = c.indexOf(name)
				if (m >= 0) {
					// We only do something if there's a match
					var la  = c.length || 0
					var lc  = name.length
					var nc  = ""
					// NOTE: This is an optimized version of the classlist. We could do
					// a simple split/join, but I *assume* this is faster.
					while (m >= 0) {
						var p = n - 1
						var n = m + lc
						// If the name is surrounded by spaces or start/end, then
						// we can remove it.
						if (((m == 0)  || (c[p] == " ")) && ((n == la) || (c[n] == " "))) {
							nc += c.substr(0, m)
						} else {
							nc += c.substr(0, m + lc)
						}
						c = c.substr(m + lc)
					}
					nc += c
					this.setAttribute("class", nc)
				}
			}
		}
		return this
	}

	static _ADD_CLASS (name) {
		// Supports giving an array of classes
		if (_.isArray(name)) {
			for (let n of name){
				this.addClass(n)
			}
			return this
		}
		// And here is the default case where we have only one argument
		if (this.classList) {
			// If the node has a classList, we use it directly
			this.classList.add(name)
		} else {
			// Otherwise we emulate it directly
			var c = this.getAttribute("class")
			if (c && c.length > 0) {
				var m   = c.indexOf(name)
				var la  = c.length || 0
				var lc  = name.length
				var p = n - 1
				var n = m + lc
				// If the name is not surrounded by spaces or start/end, then
				// we can add it
				if ( ! (((m == 0)  || (c[p] == " ")) && ((n == la) || (c[n] == " "))) ) {
					this.setAttribute (c + " " + name)
				}
			} else {
				// There is no class attribute, so we just set it
				this.setAttribute(name)
			}
		}
		return this
	}

	static _OFFSET (reference=undefined) {
		// returns the size and position of the node relative to the document
		// If a `reference` is defined it will be relative to that instead
		let box = this.getBoundingClientRect()
		let body = document.body
		let doc = document.documentElement
		let scroll_top = window.pageYOffset || doc.scrollTop || body.scrollTop
		let scroll_left = window.pageXOffset || doc.scrollLeft || body.scrollLeft
		let clientTop = doc.clientTop || body.clientTop || 0
		let clientLeft = doc.clientLeft || body.clientLeft || 0
		let top  = box.top +  scroll_top - clientTop
		let left = box.left + scroll_left - clientLeft
		if (reference){
			// NOTE: this is untested
			let offset = reference.$offset()
			top  = top  - offset.y
			left = left - offset.x
		}

		return {
			x: left,
			y: top,
			w: box.width,
			h: box.height
		}
	}

	static _FIND (query) {
		return Node.FIND(query, this)
	}

	static _FIND_MANY (query) {
		return Node.FIND_MANY(query, this)
	}

}

export class Template extends tools.Template {
// Template are used to mange lists of DOM elements

	constructor(actions={}, scope=undefined, node=undefined, anchor=undefined) {
		super(actions, scope)
		this.node      = node
		this.anchor    = anchor
	}

}

// ----------------------------------------------------------------------------
//
// PART
//
// ----------------------------------------------------------------------------
// The `part` class is a javascript representation of a section of the dom. Parts
// can be automatically bound using the class name in the `data-part` attribute in the dom,
// and parts can be composed of other parts. The part is meant to be extended with all sorts
// of custom behavior.

export class Part {
	// Is a visual subset of the application, similar to (widget or component).
	// It is tied to a node and can be made up of subParts.
	// It also manages a list of sub-elements called `nodes`.
	// Steps that happen when you bind a part:
	//  1. Part.constructor()
	//    - Events get created
	//    - Options get initialize
	//  2. Part.bind()
	//    - Nodes are bound
	//    - Sub-parts (children) are bound
	//      - Step 1 and 2 happens recursively for children
	//    - Once all children are bound the "bound" event is triggered

	static INIT_CLASS() {
		this.CLASSES = {Part:this}
		this.OPTIONS = {resize:false,load:""}
		this.INPUTS = {load:undefined}
		this.EVENTS = ["bind", "load", "resize"]
		this.FORMATTERS = {
			int             : (value) => {return format.int(value)},
			href            : format.href,
			src             : format.src,
			dataState       : format.dataState,
			dataValue       : format.dataValue,
			width           : format.width,
			height          : format.height,
			widthPercent    : format.widthPercent,
			heightPercent   : format.heightPercent,
			backgroundImage : format.backgroundImage,
			backgroundColor : format.backgroundColor,
			isSelected      : format.isSelected,
			isFocused       : format.isFocused,
			isHidden        : format.isHidden,
		}
	}

	static ADD_TYPE (part_class, name=undefined) {
		// Adds a new type of part that can be bound to
		if(!name){name=part_class.name}
		if (this.CLASSES[name]) {console.warn(`duplicate part class with name '${name}'`)}
		this.CLASSES[name] = part_class
	}

	static BIND(node, options={}){
		// Creates a part from a node and binds it,
		// if a node is already defined, it returns that
		if (node._part) {return node._part}
		let part = this.INIT(node, options)
		part.bind(node)
		return part
	}

	static BIND_PARTS(node=undefined){
		let parts = this.INIT_CHILDREN(node)
		for (let key in parts) {
			let part = tools.ensureArray(parts[key])
			for (let p of part){
				if (p) {p.bind (p.node)}
			}
		}
		return parts
	}

	static INIT(node, options={}){
		// We create a part from a node
		let [type, name] = node.$attr(MARKERS.part).split(":")
		let part_class = this.CLASSES[type]
		let part = null
		if (!part_class) {
			console.warn(`Couldn't find part with type '${type}'`)
		} else {
			part = new part_class(node, options)
		}
		return part
	}

	static INIT_CHILDREN(rootNode=Node.FIND(document.body), options={}){
		// Searches the dom for all Parts and creates them
		// we return a map of the created parts
		let nodes = this._GET_NODES(rootNode, MARKERS.part)
		let parts = {}
		for (let node of nodes) {
			// If its a template we skip it
			let is_template = ! _.isUndefined(node.$attr(MARKERS.template))
			if (is_template){
				console.warn("Part.INIT_CHILDREN: node should not also be a template")
			}
			let [type,name] = node.$attr(MARKERS.part).split(":")
			let part = this.INIT(node, options)
			if (rootNode == document.body){
				// If the rootNode is the document we add the part to the global parts
				tools.bucket(name||type, part, PARTS)
			}
			tools.bucket(name||type, part, parts)
		}
		return parts
	}

	static GET(node){
		// Returns the part for a given node if it exists
		return node._part || null
	}

	static GET_PARENT(part){
		// Recursively travel up the node to find the parent
		var node = part.node
		// We support passing a node instead of a part
		if (!node){node = part}
		let parent = node.parentElement
		if (!parent){return null}
		while (!parent._part) {
			parent = parent.parentElement
			if (!parent){return null}
		}
		if (!parent){return null}
		return parent._part
	}

	static GET_NODES(rootNode=Node.FIND(document.body)){
		// Finds the nodes and add them to the nodes map
		let nodes    = this._GET_NODES(rootNode, MARKERS.node)
		let node_map = {}
		// NOTE: we create a reference to the root node named `root`
		// this is useful when using formatters or events.
		node_map.root = rootNode
		for (let node of nodes) {
			let name = node.$attr(MARKERS.node) || "_anonymous"
			if (name == "root") {console.warn("'node' is a reserved node-id used for the part's root node")}
			tools.bucket(name, node, node_map)
		}
		return node_map
	}

	static GET_TEMPLATE_NODES(rootNode=Node.FIND(document.body)){
		// Finds the templates and adds them to the template map
		let nodes    = this._GET_NODES(rootNode, MARKERS.template)
		let node_map = {}
		for (let node of nodes) {
			let name = node.$attr(MARKERS.template) || "_anonymous"
			tools.bucket(name, node, node_map)
		}
		return node_map
	}

	static _GET_NODES(rootNode, marker){
		// Gets the nodes with the given marker that are owned by this part
		let all_nodes = rootNode.$findMany(`[${marker}]`)
		let nodes     = []
		for (let node of all_nodes) {
			let parent_node = Node.FIND_PARENT_WITH_ATTR(node, [MARKERS.part, MARKERS.template], rootNode)
			// If the parent is the rootNode or doesn't exist
			if ((!parent_node) || parent_node === rootNode) {
				nodes.push(Node.FIND(node))
			}
		}
		return nodes
	}

	static _PARSE_OPTIONS(optionString) {
		// NOTE: This is a pseudo json format using single quotes for strings and doesn't require quotes for keys
		// Reserved characters: ,:'
		// Parses the optionString into a map
		if (!optionString) { return {} }
		return eval(`({${optionString}})`)
	}

	constructor(node, options) {
		this.node       = Node.FIND(node)
		this.nodes      = {}
		this.parts      = {}
		this.templates  = {}
		this._paths     = {template:[],parts:[],nodes:[]}
		this.options    = {}
		this.data       = null // the raw data
		this.on         = {} // map of part event emitters
		this.inputs     = {} // map of part input emitters
		this.bounds     = {x:0, y:0, h:0, w:0}
		this._nodeEvents = {} // map of node/user event emitters (not really used maybe for debugging?)
		this.bindOptions(options)
		// We bind the part events
		for (let event_name of this.constructor.EVENTS){
			this.on[event_name] = new tools.Emitter()
		}
		// We bind the part inputs
		for (let event_name in this.constructor.INPUTS){
			let value = this.constructor.INPUTS[event_name]
			this.inputs[event_name] = new tools.Emitter({default:value})
		}
	}

	// ========================================================================
	// BINDING
	// ========================================================================

	bind(node=this.node) {
		this.node = Node.FIND(node)
		// We tag the dom node for easy finding
		this.node._part = this
		this.bindNodes()
		this.templates = this.bindTemplates()
		this.bindParts()
		this.bindEvents()
		if(this.options.resize){
			NodeEmitter.BIND(window, "resize", () => {this.resize ()})
		}
		this.on.bind.set(this)
		this.inputs.load.then((v)=>{this.load(v)})
		this.on.load.then((v)=>{this.process(v)})
		if(this.options.load){
			this.inputs.load.set(this.options.load)
		}
	}

	unbind() {
		// FIXME: I'm not sure if this is required or not, need to think about memory leaking, etc
		// FIXME: This doesn't remove the reference in the PARTS global (for the root part)
		// NOTE: This isn't really used much at the moment, not sure if its really needed
		delete this.node._part
		this.node = null
		this.nodes = {}
		// We unbind all the callbacks
		for (let k in this._nodeEvents){
			let emitters = this._nodeEvents[k]
			if (! _.isArray(emitters)) {emitters = [emitters]}
			for (let emitter of emitters){
				for (let event_key in emitter.events){
					emitter.remove(event_key)
				}
			}
		}
		this._nodeEvents = {}
		// We unbind all the child parts
		for (let k in this.parts){
			let parts = this.parts[k]
			if (! _.isArray(parts)) {parts = [parts]}
			for (let part of parts){
				part.unbind()
			}
		}
		this.parts = {}
	}

	bindNodes() {
		// Binds the subparts and adds them to the parts map
		this.nodes = Part.GET_NODES(this.node)
	}

	bindTemplates(root=this.node) {
		let nodes = Part.GET_TEMPLATE_NODES(root, this)
		let templates = {}
		for (let name in nodes){
			let items = tools.ensureArray(nodes[name])
			for (let node of items){
				let template = this.bindTemplate(name, node)
				tools.bucket(name, template, templates)
			}
		}
		return templates
	}

	bindTemplate(name, node){
		// We create the anchor
		let anchor = document.createComment("template:" + name)
		node.before(anchor)
		// We remove the node from the DOM
		node.remove()
		// We find the appropriate template actions or use the default actions
		let upper_name = tools.capitalize(name)
		let create_name = "template" + upper_name + "Create"
		let delete_name = "template" + upper_name + "Delete"
		let update_name = "template" + upper_name + "Update"
		let change_name = "template" + upper_name + "Render"
		let template_actions = {
			create : this[create_name] || this.defaultCreateTemplate,
			update : this[update_name] || undefined,
			delete : this[delete_name] || this.defaultDeleteTemplate,
			change : this[change_name] || this.defaultChangeTemplate
		}
		// If the change is the default the dev probably doesn't know how to use templates so we explain it
		if (template_actions.change == this.defaultChangeTemplate) {
			console.warn(`${this.constructor.name}.${change_name} handler not defined, you might also want to define ${create_name} ${update_name} ${delete_name}`)
		}
		// We create and return the new template
		return new Template (template_actions, this, node, anchor)
	}

	bindEvents() {
		// We bind the node events (user events)
		// For all the nodes we check if any events need to be bound defined using `data-do`
		let emitters = {}
		for (let node_name in this.nodes) {
			let emitter = this.bindNodeEvents(this.nodes[node_name])
			emitters[node_name] = emitter
		}
		this._nodeEvents = emitters
	}

	bindNodeEvents(nodes, skipTemplates=true){
		// bind node events can be used with nodes
		nodes = tools.ensureArray(nodes)
		let emitters = []
		for (let node of nodes){
			let is_template = !_.isUndefined(node.$attr(MARKERS.template))
			if (skipTemplates && is_template){continue}
			// We get the name of the node and the event names
			let name = node.$attr(MARKERS.node) || node.$attr(MARKERS.template)
			let event_names = node.$attr(MARKERS.events)
			if (!event_names){continue}
			event_names = event_names.split("|")
			for (let e_name of event_names){
				// We bind the event to the node using an emitter
				let emitter = new NodeEmitter(node, e_name)
				// We create a function handler name
				let handler_name = "on" + tools.capitalize(name) + tools.capitalize(e_name)
				let handler = this[handler_name]
				// We make sure a handler implemented is defined
				if (!handler){
					console.warn(`${this.constructor.name}.${handler_name} handler is missing for event '${name}'`)
				} else {
					// NOTE: We need to wrap this in a function so `this` is properly defined
					emitter.then((e) =>{
						this[handler_name](e)
					})
				}
				emitters.push(emitter)
			}
		}
		// We return a list of all the emitters
		// FIXME: Maybe we should return a cleaner more useful format
		return emitters
	}

	bindParts() {
		// Creates the parts and binds them
		this.parts = Part.BIND_PARTS(this.node)
	}

	bindOptions(options={}) {
		// Here we merge the sharedOptions, optionString and local options
		let inline_options = Part._PARSE_OPTIONS(this.node.$attr(MARKERS.options))
		this.options = tools.merge(this.constructor.OPTIONS, options, inline_options)
	}

	// ========================================================================
	// LOAD, SET AND RENDER
	// ========================================================================

	load (file) {
		// NOTE: file may be a string or an array
		// This event should be shadowed, returning a future that will have the data when set
		let emitter = Files.GET_JSON(file)
		emitter.afterAll((data)=>{
			this.on.load.set(data)
		})
	}

	process(data) {
		this.data = data
		// We trigger the event
		// this.on.set.emit(data)
		this.render(data)
	}

	render(data) {
		// The default render is to render the children
		// this.on.render.emit(data)
		this._defaultChange(data, this)
	}

	_defaultChange(data, items) {
		// items should be a map of at least {nodes,templates,parts}
		// We render the node
		for (let k in items.nodes){
			if (k == "root"){continue}
			let node = items.nodes[k]
			if (_.isArray(node)) {
				for (let n of node) {
					let path = n.$path
					if(!_.isUndefined(path)){this.format(n, tools.access(data, path))}
				}
			} else {
				let path = node.$path
				if(!_.isUndefined(path)){this.format(node, tools.access(data, path))}
			}
		}
		// We render the templates
		for (let k in items.templates){
			let template = items.templates[k]
			if (_.isArray(template)) {
				for (let t of template) {
					let path = t.node.$path
					if (!_.isUndefined(path)){t.set(tools.access(data, path))}
				}
			} else {
				let path = template.node.$path
				if(!_.isUndefined(path)){template.set(tools.access(data, path))}
			}
		}
		// We render the parts
		for (let k in items.parts){
			let part = items.parts[k]
			if (_.isArray(part)) {
				for (let p of part) {
					let path = p.node.$path
					if(!_.isUndefined(path)){p.render(tools.access(data, path))}
				}
			} else {
				let path = part.node.$path
				if(!_.isUndefined(path)){part.render(tools.access(data, part.node.$path))}
			}
		}
	}

	format (node, value, format=undefined) {
		var part_class = this.constructor
		if (!format){format = node.$format}
		let formatter = part_class.FORMATTERS[format]
		// If a formatter exists
		if (formatter) {
			let val = formatter(value, node, this.data)
			// if a value is returned we render it
			if (!_.isUndefined(val)) {
				return node.textContent = val
			}
		} else {
			// The default is to treat the value like text
			// NOTE: using innerText for performance
			node.innerText = value
		}
	}

	// ========================================================================
	// TEMPLATE
	// ========================================================================

	defaultCreateTemplate(datum, template, key, data){
		// The default template creation behavior
		let node     = Node.CLONE(template.node, template.anchor)
		let instance = node
		let part_id  = node.$attr(MARKERS.part)
		// If the template is a part we bind it and return the part
		if (part_id){
			// FIXME: Should we pass options to this bind?
			instance = Part.BIND(node)
		} else {
			// if it isn't we bind all the sub-nodes and parts and we return
			// an object containing these sub elements
			let sub_nodes     = Part.GET_NODES(node)
			let sub_templates = this.bindTemplates(node)
			let sub_parts     = Part.BIND_PARTS(node)
			let emitters      = {}
			let has_sub_elements = _.size(sub_nodes) > 1 || _.size(sub_parts) > 0 || _.size(sub_templates) > 0
			if (has_sub_elements){
				// We bind the events and save the emitters
				for (let node_name in sub_nodes) {
					let emitter = this.bindNodeEvents(sub_nodes[node_name])
					emitters[node_name] = emitter
				}
				instance = {node, nodes:sub_nodes, parts:sub_parts, templates:sub_templates, emitters}
			}
			this.bindNodeEvents(node, false)
		}
		return instance
	}

	defaultChangeTemplate(datum, instance, key, data, prevData){
		// We render the node
		if (instance instanceof Part){
			instance.render(datum)
		} else if (Node.IS_NODE(instance)){
			this.format(instance, datum)
		} else {
			this._defaultChange(datum, instance)
		}
	}

	defaultDeleteTemplate (prevData, instance, key, data) {
		if (Node.IS_NODE(instance)){
			instance.remove()
		} else if (instance instanceof Part){
			instance.node.remove()
		} else if (instance.node && Node.IS_NODE(instance.node)){
			instance.node.remove()
		}
	}

	// ========================================================================
	// USEFUL METHODS
	// ========================================================================

	measure(node) {
		return node.$offset()
	}

	getParent() {
		return Part.GET_PARENT(this)
	}

	resize() {
		this.bounds = this.measure(this.node)
		return this.bounds
	}

}
Part.INIT_CLASS()


// ----------------------------------------------------------------------------
//
// NODE EVENT EMITTER (user event emitter)
//
// ----------------------------------------------------------------------------

export class NodeEmitter extends tools.Emitter {

	static INIT_CLASS () {
		this.CUSTOM_EVENTS = {}
	}

	static ADD_CUSTOM_EVENT (name, callback) {
		// Create a custom event handler, this is a function that will get executed
		// when an event is bound to a node. The callback takes 3 parameters
		// callback(node, emitter, options). This callback should bind a function
		// to one ore many events on the node and those events should trigger the emitters
		if (this.CUSTOM_EVENTS[name]){console.warn("NodeEmitter.ADD_CUSTOM_EVENT: overriding previous custom event: " + name)}
		this.CUSTOM_EVENTS[name] = callback
	}

	static BIND (node, name, callback, options={}) {
		// Shorthand for quickly binding callbacks to nodes
		let emitter = new NodeEmitter(node, name, options)
		emitter.then(callback)
		return emitter
	}

	constructor(node, name=undefined, options={}){
		// NOTE: The node will be used as the destination
		// FIXME: We might want to support lists of nodes (for performance)
		super()
		this.node = node
		this.name = name
		this._bindNodeEvents(options)
	}

	_bindNodeEvents(options) {
		// Here we add the events to the node so they trigger when the dom
		// event triggers
		let node = this.node || this.node
		// FIXME: its currently not possible to unbind the event listeners (could be a potential memory leak?)
		let custom_event = NodeEmitter.CUSTOM_EVENTS[this.name]
		if (custom_event) {
			custom_event(node, this, options)
		} else {
			node.addEventListener(this.name, (event) => {
				this.set(event)
			}, options)
		}
	}

}
NodeEmitter.INIT_CLASS()

// ----------------------------------------------------------------------------
//
// FILE HELPER METHODS
//
// ----------------------------------------------------------------------------

export class Files {

	static GET(path) {
		// Gets a file and returns the content in a future
		let XHR = new XMLHttpRequest()
		XHR.open("GET", path, true)
		let emit = new tools.Emitter(1, 1)
		XHR.onreadystatechange = function() {
			if (XHR.readyState > 1) {
				if ((XHR.status == 200) && (XHR.readyState == 4)) {
					// SUCCESS
					return emit.set(XHR.responseText)
				} else if (XHR.readyState == 4) {
					// FAIL
					return emit.set(XHR.responseText)
				}
			}
		}
		XHR.send()
		return emit
	}

	static GET_JSON(path) {
		let is_single = _.isString(path)
		if (is_single)(path = [path])
		let size = _.size(path)
		let emit = new tools.Emitter(size, 1)
		for (let name in path){
			let p = path[name]
			this.GET(p).then((data) => {
				if (is_single){
					emit.set(JSON.parse(data))
				} else {
					emit.set(JSON.parse(data), name)
				}
			})
		}
		return emit
	}

}

export function debug () {
	// Useful for debugging
	window.PARTS   = PARTS
	window.$Part   = Part
	window.$Node   = Node
	window.$tools  = tools
	window.$format = format
}
