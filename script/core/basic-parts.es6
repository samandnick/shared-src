// -----------------------------------------------------------------------------
//
// Module: basic parts
// Status: Unfinished
//
// -----------------------------------------------------------------------------
// Basic parts is a set of sample parts and events that are very generic and will
// likely be needed on any project.

import * as tools from "./tools.js"
import * as parts from "./parts.js"

// ----------------------------------------------------------------------------
//
// CUSTOM NODE EVENTS
//
// ----------------------------------------------------------------------------
// A set of useful event wrappers that can wrap one or multiple html events and provide
// a cleaner event value.

parts.NodeEmitter.ADD_CUSTOM_EVENT("drag", (node, emitter, options) => {
	// FIXME: THIS PROBABLY CREATES MEMORY LEAKS
	let event = {
		isDown   :false,
		type     :undefined,
		target   :node,
		raw      :undefined,
		x        :undefined,
		y        :undefined,
		dx       :0,
		dy       :0,
		totalX   :0,
		totalY   :0,
		initialX :0,
		initialY :0,
	}
	let handle_start = (e) => {
		let page_x = e.pageX || e.touches[0].pageX
		let page_y = e.pageY || e.touches[0].pageY
		event.isDown   = true
		event.raw      = e
		event.initialX = page_x
		event.initialY = page_y
		event.x        = page_x
		event.y        = page_y
		event.dx       = 0
		event.dy       = 0
		event.totalX   = 0
		event.totalY   = 0
		event.type     = "down"
		emitter.set(event)
	}
	let handle_end   = (e) => {
		if (event.isDown){
			event.isDown = false
			event.raw    = e
			event.type   = "up"
			emitter.set(event)
		}
	}
	let handle_move   = (e) => {
		if (event.isDown) {
			let page_x = e.pageX || e.touches[0].pageX
			let page_y = e.pageY || e.touches[0].pageY
			event.raw    = e
			event.dx     = page_x - event.x
			event.dy     = page_y - event.y
			event.x      = page_x
			event.y      = page_y
			event.totalX = event.x - event.initialX
			event.totalY = event.y - event.initialY
			event.type   = "move"
			emitter.set(event)
		}
	}
	node.addEventListener("touchstart",  handle_start, options)
	node.addEventListener("mousedown",   handle_start, options)
	window.addEventListener("touchend",  handle_end, options)
	window.addEventListener("mouseup",   handle_end, options)
	window.addEventListener("touchmove", handle_move, options)
	window.addEventListener("mousemove", handle_move, options)
})

// ----------------------------------------------------------------------------
//
// APP PART
//
// ----------------------------------------------------------------------------
// The App part is meant to be used as a top level 'root' part. It will bind to
// a bunch of window events and properties such as resizing and url and will
// also provide useful methods for loading data.
// This part is meant to be sub-classed and extended

export class App extends parts.Part {

	static INIT_CLASS() {
		this.ADD_TYPE(this)
		this.FORMATTERS = tools.merge ({
			sizeMode: (v, n, data) => {
				let sizeMode = "small"
				if (v > 700) {
					sizeMode = "default"
				}
				n.$attr("data-size", sizeMode)
			},
		}, this.FORMATTERS)
		this.EVENTS = tools.merge ([], this.EVENTS)
	}

	resize() {
		super.resize()
		this.format(this.node, this.bounds.w, "sizeMode")
	}

}
App.INIT_CLASS()

// ----------------------------------------------------------------------------
//
// CONTROL PARTS
//
// ----------------------------------------------------------------------------

export class CheckList extends parts.Part {
// FIXME: When deselecting an element that is below the minSelection, the first available
//        element will be selected instead of doing nothing

	static INIT_CLASS () {
		this.ADD_TYPE(this)
		this.OPTIONS = {
			static       : false,
			values       : {},    // this can be an array or a map
			allToOne     : false, // TODO
			noneToAll    : false, // TODO
			minSelection : 0,
			maxSelection : -1,
		}
		this.INPUTS = tools.merge({
			selection : [],
			items     : []
		}, this.INPUTS)
		this.EVENTS = tools.merge(["selection", "items"], this.EVENTS)
	}

	constructor(node, options) {
		super(node, options)
		this.selection = {} // a map of key:boolean
	}

	setItems(data){
		this.on.items.set(data)
		this.process(data)

	}

	process(data){
		// This method will trigger the proper create/update/delete item methods for each item.
		if(_.isArray(data)) {data = tools.arrayToMap(data)}
		// We make sure the selection follows the constraints
		let values = this._validateSelection(this.getSelection())
		this._updateSelection(values, data)
		// We render the items
		this.data = data
		this.render(this.data)
	}

	setSelection (value) {
		// FIXME: We might want to use deep comparison here?
		let values = tools.ensureArray(value)
		values = this._validateSelection(values)
		this._updateSelection(values)
		// We render the result
		this.render(this.data)
	}

	bind(node) {
		super.bind(node)
		this.inputs.items.then((v)=>{this.setItems(v)})
		this.inputs.selection.then((v)=>{this.setSelection(v)})
		let values = undefined
		if(this.options.static){
			values = this.bindStaticItems()
		}else {
			values = this.options.values
		}
		this.inputs.items.set(values)
		this.setSelection (this.getSelection())
	}

	bindStaticItems(){
		let items  = tools.ensureArray(this.nodes.item)
		let values = []
		for (let item of items) {
			let value = item.$attr("data-value")
			// we update the selection if the nodes are already tagged
			this.selection[value] = item.$hasClass("is-selected")
			values.push(value)
		}
		return tools.arrayToMap(values)
	}

	getSelection() {
		return _.keys(_.pickBy(this.selection, v => v))
	}

	selectAll () {
		this.setSelection(_.keys(this.data))
	}

	isAllSelected () {
		return _.size(this.data) == _.size(_.pickBy(this.selection, v => v))
	}

	selectNone () {
		this.setSelection([])
	}

	addSelection (value) {
		let values    = tools.ensureArray(value)
		let selection = this.getSelection()
		this.setSelection(_.union(values, selection))
	}

	toggleSelection (value) {
		let values    = tools.ensureArray(value)
		let selection = this.getSelection()
		this.setSelection(_.xor(values, selection))
	}

	removeSelection (value) {
		let values    = tools.ensureArray(value)
		let selection = this.getSelection()
		this.setSelection(_.difference(selection, values))
	}

	_updateSelection(values, data=this.data){
		// updates the internal model of the selection and triggers selection event
		for (let k in this.selection){
			this.selection[k] = false
		}
		for (let v of values){
			this.selection[v] = true
		}
		// We remove any leftover keys
		for (let key of this.getSelection()){
			if (data && _.isUndefined(data[key])){
				delete this.selection[key]
			}
		}
		this.on.selection.set(values)
	}

	_validateSelection(values, data=this.data){
		// Makes sure the selection follows the rules defined in the options
		let result = values
		if (this.options.maxSelection >= 0) {
			result = values.slice(0,this.options.maxSelection)
		}
		let missing = this.options.minSelection - _.size(values)
		if (missing > 0) {
			let unselected = _.difference(_.keys(data), result)
			// We give priority to the already selected items
			let priority = _.sortBy(unselected, (v)=>{return !this.selection[v]})
			result = _.union(priority.slice(0, missing), result)
		}
		return result
	}

	render (data) {
		// We render the result
		if (this.options.static){
			for (let node of tools.ensureArray(this.nodes.item)){
				let key = node.$attr("data-value")
				node.$toggleClass("is-selected", this.selection[key])
			}
		} else {
			this.templates.item.set(data)
		}
	}

	templateItemRender(datum, instance, key){
		instance.node.$attr("data-value", key)
		instance.nodes.label.$text(datum)
		instance.node.$toggleClass("is-selected",this.selection[key] || false)
	}

	onItemClick(event) {
		let value = event.currentTarget.getAttribute("data-value")
		if (_.isUndefined(value)){console.warn("item needs data-value attribute")}
		this.toggleSelection(value)
	}

	onAllClick(event) {
		this.selectAll()
	}

	onNothingClick(event) {
		this.selectNone()
	}

}
CheckList.INIT_CLASS()

export class NiceCheckList extends CheckList {
// Adds nice animation and sorted elements to the checklist

	static INIT_CLASS () {
		this.ADD_TYPE(this)
		this.OPTIONS = tools.merge({
			itemHeight : 28
		},this.OPTIONS)
	}

	bind () {
		super.bind()
	}

	render (data) {
		// We sort the items based on the selection
		let keys = _.map(data, (v) => v.key)
		// We sort the data
		keys = _.sortBy(keys, (k) => {
			if (this.selection[k]) {
				return 0
			} else {
				return 1
			}
		})
		// We update the data
		// NOTE: We should make a copy and not mutate the data
		for(let index in keys){
			let key = keys[index]
			data[key].index= index
		}
		this.nodes.items.$css("height", _.size(data) * this.options.itemHeight)
		this.templates.item.set(data)
	}

	process(data) {
		let items = tools.items(data)
		// We create the new item objects with index
		let itemData = {}
		for (let entry of items.entries()) {
			let item = entry[1]
			let index = entry[0]
			itemData[item.k] = {
				label: item.v,
				index: index,
				key  : item.k,
			}
		}
		super.process(itemData)
	}

	templateItemRender(datum, instance, key){
		instance.node.$attr("data-value", key)
		instance.node.$css("top", datum.index * this.options.itemHeight)
		instance.nodes.label.$text(datum.label)
		instance.node.$toggleClass("is-selected",this.selection[key] || false)
	}

	templateItemCreate(datum, instance, key, data) {
		instance = this.defaultCreateTemplate(datum, instance, key, data)
		instance.node.$addClass("is-new")
		setTimeout(()=> {
			instance.node.$removeClass("is-new")
		},100)
		return instance
	}

	templateItemDelete(prevData, instance, key, data) {
		setTimeout(()=> {
			instance.node.$addClass("is-new")
		}, 250)
		instance.node.remove()

	}

}
NiceCheckList.INIT_CLASS()

export class Slider extends parts.Part {
// Slider value selector or range selector
// TODO: Add anchor/label support

	static INIT_CLASS () {
		this.ADD_TYPE(this)
		this.OPTIONS = {
			vertical : false,
			values   : [0],
			marks    : [],
			ranges   : [],
			anchors  : [],
			range    : [0, 1],
			resize   : true,
		}
		this.EVENTS = tools.merge(["select"], this.EVENTS)
	}

	constructor(node, options) {
		super(node, options)
		this.values  = []
		this.ranges  = {}
		this.cache   = {
			bar: undefined
		}
		this.range   = _.clone(this.options.range)
	}

	bind (node){
		super.bind(node)
		this.resize()
		this.setValues(this.options.values)
		this.setRanges(this.options.ranges)
	}

	setRange (range){
		this.range = [Math.min(range[0], range[1]), Math.max(range[0], range[1])]
		this.setValues()
	}

	setValues (values=this.values){
		values = tools.ensureArray(values)
		// We validate the values
		values = _.map(values, (v) => tools.clamp(v,this.range[0], this.range[1]))
		values = _.sortBy(values)
		this.values = values
		this.templates.handle.set(values)
		// We update the ranges (in case any are now invalid)
		this.setRanges ()
		this.on.select.set(this.values)
	}

	getValue (index=0){
		return this.values[index]
	}

	getValues (){
		return this.values
	}

	setRanges(ranges=this.ranges){
		if (! ranges[0] instanceof Array) {ranges = [ranges]}
		// We update the ranges (removing any that aren't valid)
		let last = this.values.length - 1
		ranges = _.filter(ranges, (v) => {
			return v[0] <= last && v[1] <= last
		})
		this.ranges = ranges
		this.templates.range.set(ranges)
	}

	resize () {
		this.cache.bar = this.nodes.bar.$offset()
		super.resize()
	}

	_projectDragValue(value){
		let result = null
		let min   = null
		let max   = null
		let dimensions = this.cache.bar
		if (this.options.vertical){
			min = dimensions.y
			max = dimensions.y + dimensions.h
		} else {
			min = dimensions.x
			max = dimensions.x + dimensions.w
		}
		result = tools.scale(value, [min,max], this.range)
		// result = tools.clamp(result, this.range[0], this.range[1])
		return result
	}

	onHandleDrag(event){
		let position = event.x
		if (this.options.vertical){position = event.y}
		let index = event.target.getAttribute("data-index")
		let values = _.clone(this.values)
		let value = this._projectDragValue (position)
		values[index] = value
		this.setValues(values)
	}

	onRangeDrag(event){
		let position = event.x
		let delta    = event.dx
		let size     = this.bounds.w
		let index = event.target.getAttribute("data-index")
		if (this.options.vertical){
			position = event.y
			delta    = event.dy
			size     = this.bounds.h
		}
		let range = this.ranges[index]
		let range_width = this.range[1] - this.range[0]
		let offset = tools.scale(delta,[0,size],[0,range_width])
		let values = _.clone(this.values)
		values[range[0]] = values[range[0]] + offset
		values[range[1]] = values[range[1]] + offset
		this.setValues(values)
	}

	templateHandleRender (datum, instance, key, data) {
		var property = "left"
		if (this.options.vertical){property = "top"}
		let offset = tools.scale(datum, this.range, [0,100])
		instance.node.$css(property, offset + "%")
	}

	templateHandleCreate (datum, instance, key, data) {
		instance = this.defaultCreateTemplate(datum, instance, key, data)
		instance.node.$attr("data-index", key)
		return instance
	}

	templateRangeRender (datum, instance, key, data) {
		var offset_prop = "left"
		var size_prop   = "width"
		if (this.options.vertical){
			offset_prop = "top"
			size_prop   = "height"
		}
		let start = this.values[datum[0]]
		let end   = this.values[datum[1]]
		let offset = tools.scale(start, this.range, [0,100])
		let size   = tools.scale(end - start, this.range, [0,100])
		instance.node.$css(offset_prop, offset + "%")
		instance.node.$css(size_prop, size + "%")
	}

	templateRangeCreate (datum, instance, key, data) {
		instance = this.defaultCreateTemplate(datum, instance, key, data)
		instance.node.$attr("data-index", key)
		return instance
	}

}
Slider.INIT_CLASS()
