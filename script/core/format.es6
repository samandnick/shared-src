// -----------------------------------------------------------------------------
//
// Module: format
// Status: Unfinished
//
// -----------------------------------------------------------------------------
// A collection of string and node formatters to be used as the parts.FORMATTERS

// ============================================================================
// TEXT FORMATTERS
// ============================================================================

export function int(value, sep=",") {
	value = "" + Math.round(value)
	let result = ""
	let len = value.length
	for(let i=0; i < value.length; i+=1){
		let letter = value[i]
		if ((len-i)%3 == 0 && i != 0 && value[i-1] != "-"){
			result += sep
		}
		result += letter
	}
	return result
}

// ============================================================================
// NODE FORMATTERS
// ============================================================================

// content

export function html(value, node){
	node.innerHTML = value
}

export function text(value, node){
	node.textContent = value
}

// attribute

export function href(value, node){
	node.$attr("href", value)
}

export function src(value, node){
	node.$attr("src", value)
}

export function dataState(value, node){
	node.$attr("data-state", value)
}

export function dataValue(value, node){
	node.$attr("data-value", value)
}

// style

export function width(value, node){
	node.$css("width", value)
}

export function height(value, node){
	node.$css("width", value)
}

export function widthPercent(value, node){
	node.$css("width", value + "%")
}

export function heightPercent(value, node){
	node.$css("width", value + "%")
}

export function backgroundImage(value, node){
	node.$css("backgroundImage", "url(" + value + ")")
}

export function backgroundColor(value, node){
	node.$css("backgroundColor", value)
}

// class

export function isSelected(value, node){
	node.$toggleClass("is-selected", value)
}

export function isFocused(value, node){
	node.$toggleClass("is-focused", value)
}

export function isHidden(value, node){
	node.$toggleClass("is-hidden", value)
}
