
var CONFIG = {
	VERSION     : "M0",
	USER        : "ubuntu",
	SERVER      : "www.build.run",
	SERVER_PATH : `/var/www/html` // For the root website,
}

var date    = new Date ()
var DATE    = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()

// ============================================================================
// CUSTOM PLUGINS
// ============================================================================

var through = require("through2");
var PluginError = require("plugin-error");
var cheerio = require("cheerio")
var path    = require("path")

function injectSystemjs () {
	return through.obj(function(file, encoding, callback) {
		// We get the depth of the file currently being processed
		let depth = file.relative.split(path.sep).length - 1
		// We convert that to a path adjustment
		let path_to_root = "./" + new Array(depth).fill("../").join("")
		if (file.isStream()){
			return callback(new PluginError)
		}
		if (file.isBuffer()){
			try {
				var contents = String(file.contents);
				// We parse the html
				var $ = cheerio.load(contents)
				// Find the module script tags
				// remove the module script tags and add the systemjs script
				var scripts = []
				$("script[type=\"module\"]").each((i,node) => {
					node = $(node)
					var url = node.attr("src")
					node.remove()
					scripts.push(url)
				})
				// Finally add the system import script
				if (scripts.length > 0){
					$("head").append(`<script src="${path_to_root}script/system-production.js"></script>`)
					var imports = scripts.map((v) => {return `System.import("${v}");`}).join("\n")
					var config  = `System.config({"baseURL":"./"});`
					$("head").append(`<script>${config}\n${imports}</script>`)
				}
				file.contents = new Buffer($.html());
			} catch(e){
				return callback(new PluginError("injectSystemjs", e))
			}
			callback(null, file);
		}
	});
};

function renamePartAttributes () {

	const MARKERS = {
		part       : "data-part",
		node       : "data-node",
		template   : "data-template",
		options    : "data-options",
		format     : "data-format",
		on         : "data-on",
		path       : "data-path",
	}

	return through.obj(function(file, encoding, callback) {
		if (file.isStream()){
			return callback(new PluginError)
		}
		if (file.isBuffer()){
			try {
				var contents = String(file.contents);
				// We parse the html
				var $ = cheerio.load(contents)
				// For each marker we find the related nodes
				for (let key in MARKERS) {
					let marker = MARKERS[key]
					// For each node we remove the original attribute and replate it
					let nodes = $(`[${key}]`).each((i,node) => {
						node = $(node)
						let value = node.attr(key)
						node.attr(key,null)
						node.attr(marker, value)
					})
				}
				file.contents = new Buffer($.html());
			} catch(e){
				return callback(new PluginError("renamePartAttributes", e))
			}
			callback(null, file);
		}
	});
};

var {exec} = require("child_process")

function runCommand (command) {
	return through.obj(function(file, encoding, callback) {
		// FIXME: user proper gulp logging
		console.log(command)
		exec(command, (err,stdout,stderr)=> {
			if (stdout) {console.log(stdout)}
			if (stderr) {console.log(stderr)}
			callback(null, file);
		})
	});
};

// ============================================================================
// INCLUDES
// ============================================================================

var gulp        = require("gulp")
var path        = require("path")
var del         = require("del")
var sass        = require("gulp-sass")
var pug         = require("gulp-pug")
var connect     = require("gulp-connect")
var babel       = require("gulp-babel")
var rename      = require("gulp-rename")
let cleanCSS    = require("gulp-clean-css")
var uglify      = require("gulp-uglify")
var rsync       = require("gulp-rsync")
var cache       = require("gulp-cached")
var fs          = require("fs")
var path        = require("path")

var SOURCE_DIR         = "src"                  // Directory where the unprocessed files are
var SHARED_DIR         = "shared-src"           // Directory of unprocessed shared files
var DIST_DIR           = "dist"                 // Where to dump the files (for development)
var BUILD_DIR          = "builds"               // Where to place the builds
var BLACKLIST          = "!" + SOURCE_DIR + "/**/*.part.*" // directories with this name will be ignored
var SHARED_BLACKLIST   = "!" + SHARED_DIR + "/**/*.part.*" // directories with this name will be ignored
var GULPFILE           = "!" + SHARED_DIR + "/gulpfile.js"
function prefix_all(paths){
	let result = []
	for (let path of paths){
		result.push(SOURCE_DIR + path)
		result.push(SHARED_DIR + path)
	}
	return result
}

function prefix(paths){
	let result = prefix_all(paths)
	result.push(BLACKLIST)
	result.push(SHARED_BLACKLIST)
	result.push(GULPFILE)
	return result
}

let JS_PATH      = "/**/*.js"
let ES6_PATH     = "/**/*.es6"
let CSS_PATH     = "/**/*.css"
let SASS_PATH    = "/**/*.sass"
let PUG_PATH     = "/**/*.pug"
let ASSETS_PATH  = "/**/*.*" // any file

// NOTE: file types are not restricted to any folder structure and can be placed wherever.
var JS_DIR         = prefix     ([JS_PATH])
var JS_DIR_ALL     = prefix_all ([JS_PATH])
var ES6_DIR        = prefix     ([ES6_PATH])
var ES6_DIR_ALL    = prefix_all ([ES6_PATH])
var CSS_DIR        = prefix     ([CSS_PATH])
var CSS_DIR_ALL    = prefix_all ([CSS_PATH])
var SASS_DIR       = prefix     ([SASS_PATH])
var SASS_DIR_ALL   = prefix_all ([SASS_PATH])
var PUG_DIR        = prefix     ([PUG_PATH])
var PUG_DIR_ALL    = prefix_all ([PUG_PATH])

// Assets are any file not supported by the other rules
var ASSET_FILTER = [
	SHARED_DIR + ASSETS_PATH,
	SOURCE_DIR + ASSETS_PATH,
	"!" + SOURCE_DIR + JS_PATH,
	"!" + SHARED_DIR + JS_PATH,
	"!" + SOURCE_DIR + ES6_PATH,
	"!" + SHARED_DIR + ES6_PATH,
	"!" + SOURCE_DIR + CSS_PATH,
	"!" + SHARED_DIR + CSS_PATH,
	"!" + SOURCE_DIR + SASS_PATH,
	"!" + SHARED_DIR + SASS_PATH,
	"!" + SOURCE_DIR + PUG_PATH,
	"!" + SHARED_DIR + PUG_PATH
]

// FIXME: Think about what could happen if .js,.css,.sass files are in the assets folder? will they get processed twice?
var ASSETS_DIR     = ASSET_FILTER
var ASSETS_DIR_ALL = ASSET_FILTER

// ============================================================================
// HELPER FUNCTIONS
// ============================================================================

function delete_file(event){
	var filePathFromSrc = path.relative(path.resolve(SOURCE_DIR), event);
	var destFilePath    = path.resolve(DIST_DIR, filePathFromSrc);
	del.sync(destFilePath);
}

function log_error (error){
	console.log(error.message)
	this.emit("end")
}

function load(filePath){
	filePath = path.join(__dirname, filePath);
	return JSON.parse(fs.readFileSync(filePath, {encoding:"utf8"}))
}

// ============================================================================
// DIST RULES
// ============================================================================

// Compile CSS
gulp.task("css", () => {
	return gulp.src(CSS_DIR)
		.pipe(cache("cache"))
		.pipe(gulp.dest(DIST_DIR))
});

// Compile Sass
gulp.task("sass", () => {
	return gulp.src(SASS_DIR)
		.pipe(sass())
		.on("error", log_error)
		.pipe(cache("cache"))
		.pipe(gulp.dest(DIST_DIR))
});

// We just move the javascript files for now
gulp.task("es6", () => {
	return gulp.src(ES6_DIR)
		.pipe(rename({
			extname: ".js"
		}))
		.on("error", log_error)
		.pipe(cache("cache"))
		.pipe(gulp.dest(DIST_DIR))
});

// We just move the javascript files for now
gulp.task("javascript", () => {
	return gulp.src(JS_DIR)
		.pipe(cache("cache"))
		.pipe(gulp.dest(DIST_DIR))
});

// Compile Pug
gulp.task("pug", () => {
	return gulp.src(PUG_DIR)
		.pipe(pug({data:{load:load}}))
		.on("error", log_error)
		.pipe(renamePartAttributes())
		.on("error", log_error)
		.pipe(cache("cache"))
		.pipe(gulp.dest(DIST_DIR))
});

// We move the assets
gulp.task("assets", () => {
	return gulp.src(ASSETS_DIR)
		.pipe(cache("cache"))
		.pipe(gulp.dest(DIST_DIR))
});

// Clean (removes the dist directory)
gulp.task("clean", () =>  {
	return del(["./" + DIST_DIR])
});

// ============================================================================
// BUILD RULES
// ============================================================================

// Compile CSS
gulp.task("build-css", () => {
	return gulp.src(CSS_DIR)
		.pipe(cleanCSS())
		.pipe(gulp.dest(`${BUILD_DIR}/${CONFIG.VERSION}-${DATE}`));
});

// Compile Sass
gulp.task("build-sass", () => {
	return gulp.src(SASS_DIR)
		.pipe(sass())
		.pipe(cleanCSS())
		.pipe(gulp.dest(`${BUILD_DIR}/${CONFIG.VERSION}-${DATE}`));
});

// We just move the javascript files for now
gulp.task("build-es6", () => {
	return gulp.src(ES6_DIR)
		.pipe(babel({
			plugins:["babel-plugin-transform-es2015-modules-systemjs"],
			presets:["env"]
		}))
		.pipe(rename({
			extname: ".js"
		}))
		.pipe(uglify({mangle:false}))
		.pipe(gulp.dest(`${BUILD_DIR}/${CONFIG.VERSION}-${DATE}`));
});

// We just move the javascript files for now
gulp.task("build-javascript", () => {
	return gulp.src(JS_DIR)
		.pipe(uglify())
		.pipe(gulp.dest(`${BUILD_DIR}/${CONFIG.VERSION}-${DATE}`));
});

// Compile Pug
gulp.task("build-pug", () => {
	// We move the files to the root of the dist directory
	return gulp.src(PUG_DIR)
		.pipe(pug({data:{load:load}}))
		.pipe(renamePartAttributes())
		.pipe(injectSystemjs())
		.pipe(gulp.dest(`${BUILD_DIR}/${CONFIG.VERSION}-${DATE}`));
});

// We move the assets
gulp.task("build-assets",  () => {
	return gulp.src(ASSETS_DIR)
		.pipe(gulp.dest(`${BUILD_DIR}/${CONFIG.VERSION}-${DATE}`));
});

// Clean (removes the dist directory)
gulp.task("build-clean",  () =>  {
	return del(["./" + `${BUILD_DIR}/${CONFIG.VERSION}-${DATE}`])
});

// ============================================================================
// WEBSERVER RULES
// ============================================================================


// Watch Files For Changes
gulp.task("watch", () => {
	gulp.watch(JS_DIR_ALL,     gulp.series("javascript")).on("unlink", delete_file)
	gulp.watch(ES6_DIR_ALL,    gulp.series("es6"))
	gulp.watch(SASS_DIR_ALL,   gulp.series("sass"))
	gulp.watch(CSS_DIR_ALL,    gulp.series("css"))
	gulp.watch(PUG_DIR_ALL,    gulp.series("pug"))
	gulp.watch(ASSETS_DIR_ALL, gulp.series("assets")).on("unlink", delete_file)
	gulp.watch(DIST_DIR).on("change", (file) => {
		// NOTE: this seems to trigger for all the files of a given glob: https://github.com/paulmillr/chokidar/issues/646
		console.log("reaload", file)
		gulp.src(file).pipe(connect.reload())
	})
	// NOTE: We might want to wait untill all the files are done before reloading
	//       with gulp-watch something like the following was recommended.
	// var timer = null;
	// function reload (file) {
	// 	if (timer) clearTimeout(timer);
	// 	if (!gulp.isRunning) {
	// 		timer = setTimeout ( () =>  {
	// 			gulp.src(file).pipe(connect.reload())
	// 		}, 250)
	// 	}
	// }
});

gulp.task("webserver", () => {
	connect.server({
		root: DIST_DIR,
		port: 8000,
		livereload: true,
		index: "./"
	});
});

gulp.task("build-webserver", () => {
	connect.server({
		root: `${BUILD_DIR}/${CONFIG.VERSION}-${DATE}`,
		port: 8000,
		livereload: true,
		index: "./"
	});
});


gulp.task("build", gulp.series("build-clean", gulp.parallel("build-pug", "build-sass", "build-javascript", "build-es6", "build-css", "build-assets")));

gulp.task("dist", gulp.series("clean", gulp.parallel("pug", "sass", "javascript", "es6", "css", "assets")));

gulp.task("deploy", gulp.series("build", () => {
	rsyncConf = {
		archive          : true,
		compress         : true,
		progress         : true,
		incremental      : true,
		emptyDirectories : true,
		recursive        : true,
		exclude          : [],
		hostname         : CONFIG.SERVER,
		username         : CONFIG.USER,
		destination      : CONFIG.SERVER_PATH,
		chmod            : "ugo=rwX",
		root             : `${BUILD_DIR}/${CONFIG.VERSION}-${DATE}`
	}
	return gulp.src(`${BUILD_DIR}/${CONFIG.VERSION}-${DATE}`)
		.pipe(runCommand(`ssh ${CONFIG.USER}@${CONFIG.SERVER} mkdir -p ${CONFIG.SERVER_PATH}`))
		.pipe(rsync(rsyncConf))
}))

// Default Task (creates the dist, starts a webserver and watches the files)
gulp.task("default", gulp.series("dist", gulp.parallel("webserver", "watch")));

function config(configuration){
	CONFIG = configuration
}

module.exports = {gulp:gulp, config:config}
