/*
 * decaffeinate suggestions:
 * DS001: Remove Babel/TypeScript constructor workaround
 * DS206: Consider reworking classes to avoid initClass
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// -----------------------------------------------------------------------------
//
// Module: animation
// Status: API design
//
// -----------------------------------------------------------------------------

// CURRENT ISSUES WITH DESIGN
// - How to handle infinite looping tweens in timelines (should it just not count to total timeline length?)
// - What's the relationship between Timeline and Animator (inheritance, composition?)
// - Should `Transition` have callback functions?
// - Should `Transition` have flow controls (play, pause, restart)
// - Should `Timeline` inherit from `Transition` (duration behaves differently tho)

class Easing {
	static initClass() {
	// |     .-.
	// |    /   \         .-.
	// |   /     \       /   \       .-.     .-.     _   _
	// 0--/-------\-----/-----\-----/---\---/---\---/-\-/-\/\/--1
	// | /         \   /       \   /     '-'     '-'
	// |/           '-'         '-'
	// A list of functions that will return a numeric value related to the input k
	// which is a value between 0 and 1

		this.LINEAR = k => {};
		this.EASE_IN = k => {};
		this.EASE_OUT = k => {};
		this.EASE_IN_OUT = k => {};
	}
}
Easing.initClass();

class Stagger {
	static initClass() {
	// A list of Stagger Values

		this.LIST = (index, element, staggerDelay, properties) => {
			if (properties == null) { properties = {startIndex:0}; }
		};
		// Returns a delay based on the element index and startIndex
		this.GRID = (index, element, staggerDelay, properties) => {
			if (properties == null) { properties = {startIndex:0, columnCount:0}; }
		};
		// Returns a delay based on the element index and startIndex (as if they where in a grid)
		this.RANDOM = (index, element, staggerDelay, properties) => {
			if (properties == null) { properties = {margin:250}; }
		};
	}
}
Stagger.initClass();
	// Returns a random delay around the staggerDelay, (based on the margin)


class Tween {
	static initClass() {
	// Is a transition applied to a specific target/scope
	// Transition in an Interpolation over time and provides many timing features such as repetition and yoyo
	// It is not standalone however and requires manual manipulation of `render` function

		this.OPTIONS = {
			ease            : Easing.LINEAR,
			delay           : 0,     // Number of seconds to wait before triggering the Tween (increases the overall duration)
			repeat          : 0,     // Number of times to repeat -1 being infinite
			repeatDelay     : 0,     // Delay between repetitions (`delay` only gets applies on the first loop)
			duration        : 1,     // Number of seconds for the tween to play
			reverse         : false, // Initial direction of the Tween
			yoyo            : false, // causes every other repetition to be reversed
			paused          : false, // Pause the tween when it is created
			useFrames       : false, // UNDER CONSIDERATION: animates over frames instead of time
			immediateRender : false, // UNDER CONSIDERATION: Render immediately instead of waiting for render cycle
			onCollision     : (state,target,collision) => ({}), // UNDER CONSIDERATION: Triggers when 2 tweens interpolate over the same values for a given target, returns the collided values
			onUpdate        : (state,target) => ({}),
			onRepeat        : (state,target) => ({}), // UNDER CONSIDERATION:
			onComplete      : (state,target) => ({}),
			animator        : null
		};

		this.APPLY = (target, tween, callbacks) => {
			if (callbacks == null) { callbacks = True; }
		};
		// Apply a tween to an element (clones the tween)

		this.CLONE = (tween, callbacks) => {
			if (callbacks == null) { callbacks = True; }
		};
		// Clones the entire tween preserving everything, but the target (optionally ommitting callbacks)

		this.FROM_TO = (target, fromToState, options) => {};
	}
	// Alternate notation for constructor, allowing to defined from/to state in the following manner:
	// fromTo (tui, {
	//   top:     [0, 100]
	//   opacity: [0,1]
	// }, {duration: 1})
	// Which is a bit more compact in a lot of circumstances

	constructor(target, fromState, toState, options) {
		this.seek = this.seek.bind(this);
		this.setOptions = this.setOptions.bind(this);
		this.setOption = this.setOption.bind(this);
		this.bind = this.bind.bind(this);
		this.rebind = this.rebind.bind(this);
		this.to = this.to.bind(this);
		this.render = this.render.bind(this);
		this.get = this.get.bind(this);
		this.restart = this.restart.bind(this);
		this.reverse = this.reverse.bind(this);
		this.setProgress = this.setProgress.bind(this);
		this.play = this.play.bind(this);
		this.stop = this.stop.bind(this);
		this.pause = this.pause.bind(this);
		this.targets  = [];
		this.animator = null;
		this.fromState = fromState;
		this.toState   = toState;
		this.easing    = None;
	}

	seek(time) {}
	// Sets the Transition to the seeked time

	setOptions(options) {}
	// Updates the value of a set of options

	setOption(option, value) {}
	// Updates the value of a set of options

	bind(target) {}
	// NOTE: This is if a tween can support more than one target at a time
	// Binds the tween to an additional target (preserving the current targets)

	rebind(target) {}
	// Removes the current target and replaces it with the passed one(s).

	to(toState, options) {}
	// Animates elements from the current state to the next one

	render(elapsed) {}
	// Runs callback functions, calculates the currentState

	get(progress) {}
	// Computes the interpolated value based on the progress between 0 and 1

	// =========================================================================
	// TIME FLOW CONTROLS
	// =========================================================================

	restart() {}
	reverse() {}

	setProgress(progress) {}
	// Same as seek but with a value between 0 and 1

	play() {}
	// Adds the tween to the animator that will call the _render function
	stop() {}
	pause() {}
}
Tween.initClass();

class Timeline extends Tween {
	static initClass() {

		this.OPTIONS = {
			duration        : -1    // Number of seconds for the Timeline `-1` lets the duration be based off of the children
		};
	}
	// NOTE: All the options are the same as transition except the duration is -1

	constructor(options) {
		{
		  // Hack: trick Babel/TypeScript into allowing this before super.
		  if (false) { super(); }
		  let thisFn = (() => { this; }).toString();
		  let thisName = thisFn.slice(thisFn.indexOf('{') + 1, thisFn.indexOf(';')).trim();
		  eval(`${thisName} = this;`);
		}
		this.add = this.add.bind(this);
		this.remove = this.remove.bind(this);
		this.play = this.play.bind(this);
		this.stop = this.stop.bind(this);
		this.pause = this.pause.bind(this);
		this.restart = this.restart.bind(this);
		this.reverse = this.reverse.bind(this);
		this.seek = this.seek.bind(this);
		this.setProgress = this.setProgress.bind(this);
		super(undefined, 0, 1, options);
		this.animator;
	}

	add(tween, delay, duration, target) {
		if (target == null) { target = None; }
	}
	// Adds a Tween to the timeline that will play after a set delay.
	// Adding a Tween to a timelines gives total control to the timeline
	// trying to manipulate the flow of the children will trigger warnings and will do nothing
	// NOTE: duration will modify the duration property of the tween

	remove(tween) {}
	// Removes the tween from the timeline

	// =========================================================================
	// TIME FLOW CONTROLS
	// =========================================================================

	play() {}
	stop() {}
	pause() {}
	restart() {}
	reverse() {}
	seek(time) {}
	// Sets the Timeline to the seeked time
	setProgress(progress) {}
}
Timeline.initClass();
	// Same as seek but with a value between 0 and 1

({
	tween: (target, fromState, toState, options) => {},
	// Shorthand for `new Tween`

	fromTo: (target, fromToState, options) => {},
	// Shorthand for `Tween fromTo`

	staggerTimeline: (tweens, staggerCallback, staggerDelay, staggerProperties, options) => {},
	// Creates a new timeline adding the tweens with delays based on the staggerCallback

	timeline: (tweenGroup, options) => {}
});
// Creates a new timeline based on the list of tweens
// tweenGroup = [
//     [tween1, delay, <duration>]
//     [tween2, delay, <duration>]
// ]

class Animator {
	static initClass() {
	// An animator controls the flow of a collection of Animators/Tweens,

		this.OPTIONS = {
			useFrames       : false // animates over frames instead of time
		};

		this.ANIMATOR = null;
	}

	constructor(options) {
		this.add = this.add.bind(this);
		this.render = this.render.bind(this);
		this.play = this.play.bind(this);
		this.pause = this.pause.bind(this);
		this.children;
		this._lastTime;
		this._progress;
	}

	add(transition) {}
	// Adding the tween/timeline to the animator will start rendering the Tween
	// the moment it is added.

	render() {}
	// Triggers the children callback with a suitable elapsed value
	// This method will be called in an event loop such as `requestAnimationFrame`

	// =========================================================================
	// TIME FLOW CONTROLS
	// =========================================================================

	play() {}
	pause() {}
}
Animator.initClass();

Animator.ANIMATOR = new Animator();


window.animation = {
	Easing,
	Stagger,
	Tween,
	Animator,
	Timeline
};

// EOF
