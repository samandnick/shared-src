# ABOUT

The shared-src is a common repository that all projects share.
it is where all the reusable code lives and includes tests to ensure
that they are working as expected. This repository should only be pulled
as a sub-repository.

If you want to start a new project fork `project-template` and clone is using the
`--recursive` tag. This project will automatically pull the `shared-src` repository.