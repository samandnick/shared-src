import * as tools    from "../../script/core/tools.js"
import * as parts    from "../../script/core/parts.js"
import * as basic from "../../script/core/basic-parts.js"

export function start () {
	parts.Part.BIND_PARTS()
}

export class DynamicTest extends parts.Part {

	static INIT_CLASS () {
		this.ADD_TYPE(this)
	}

	bind (node) {
		super.bind(node)
	}

	onButtonClick () {
		let items = {
			a : "Item A",
			b : "Item B",
			c : "Item C",
			d : "Item D",
			e : "Item E",
			f : "Item F",
			g : "Item G",
			h : "Item H",
		}
		items = _.pickBy(items, (v) => {
			if (Math.random () > 0.2) {return true}
		})
		this.parts.NiceCheckList.inputs.items.set(items)
	}
}
DynamicTest.INIT_CLASS()

export class DoubleBoundChecklist extends parts.Part {

	static INIT_CLASS () {
		this.ADD_TYPE(this)
	}

	bind (node) {
		super.bind(node)
		// If one checklist is updated than we update the other one
		let checklist = this.parts.NiceCheckList[0]
		let checklist2 = this.parts.NiceCheckList[1]
		let tags      = this.parts.CheckList
		checklist .on.selection.then(checklist2.inputs.selection)
		checklist2 .on.selection.then(checklist.inputs.selection)
		// tags      .on.afterSelect.then(checklist.on.selection)
		// tags      .on.afterSelect.then(checklist2.on.selection)
		checklist2 .on.items .then(checklist.inputs.items)
		checklist .on.items .then(checklist2.inputs.items)
		// tags      .on.afterSet .then(checklist.on.items)
		// tags      .on.afterSet .then(checklist2.on.items)
		// TODO: update format to
		// events.JOIN({
		// 	select: checklist.on.selection,
		// 	select: tags.on.selection,
		//  }, tags.on.both)
	}

	onButtonClick () {
		let items = {
			a : "Item A",
			b : "Item B",
			c : "Item C",
			d : "Item D",
			e : "Item E",
			f : "Item F",
			g : "Item G",
			h : "Item H",
		}
		items = _.pickBy(items, (v) => {
			if (Math.random () > 0.2) {return true}
		})
		this.parts.NiceCheckList[0].inputs.items.set(items)
	}

	onButton2Click () {
		let items = {
			a : "Item A",
			b : "Item B",
			c : "Item C",
			d : "Item D",
			e : "Item E",
			f : "Item F",
			g : "Item G",
			h : "Item H",
		}
		items = _.pickBy(items, (v) => {
			if (Math.random () > 0.2) {return true}
		})
		this.parts.NiceCheckList[1].inputs.items.set(items)
	}
}
DoubleBoundChecklist.INIT_CLASS()

start ()
