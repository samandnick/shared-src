import * as tools    from "../../script/core/tools.js"
import * as parts    from "../../script/core/parts.js"

let DATA = {
	title    : "part title",
	subtitle : "part subtitle",
	letters  : ["a","b","c","d"],
	child:{
		title: "child part title",
		subtitle: "child part title"
	},
	table:[
		["name", "country", "color"],
		["sam", "canada", "red"],
		["nick", "argentina", "blue"],
	],
	colors:["red","green","blue","purple"]
}

export function start () {
	parts.Part.BIND_PARTS()
}

export class SamplePart extends parts.Part {

	static INIT_CLASS(){
		this.ADD_TYPE(this)
	}

	bind (node) {
		super.bind(node)
		this.set(DATA)
	}

}
SamplePart.INIT_CLASS()

start ()

parts.debug()