import * as tools from "../../script/core/tools.js"
import Vue        from "../../script/vue-2.5.16.js"

Vue.component("check-list", {
	props:{
		defaultItems : {
			type: Array,
			default: () => {return [
				{key:"a", label:"default a", selected:false},
				{key:"b", label:"default b", selected:false},
				{key:"c", label:"default c", selected:false},
			]}
		}
	},
	methods: {
		toggleItem : function (event) {
			let item_key = event.currentTarget.getAttribute("data-key")
			let item     = _.find(this.items, (v) => {return v.key == item_key})
			item.selected = !item.selected
			this.updateItems(this.items)
		},
		updateItems: function (items){
			this.items = items
			// Do We need to deep copy the items?
			this.$emit("update-items", _.cloneDeep(items))
		}
	},
	data: function () {
		return {
			items: _.cloneDeep(this.defaultItems)
		}
	},
	template: "#check-list"
})

var app = new Vue ({
	el: "#app",
	data: {
		items: [
			{key:"a", label:"item a", selected:true},
			{key:"b", label:"item b", selected:true},
			{key:"c", label:"item c", selected:false},
			{key:"d", label:"item d", selected:false},
		]
	},
	methods: {
		updateItems: function (items) {
			this.items = items
		}
	}
})

var checklist = new Vue ({
	el: "#standalone-checklist",
})
